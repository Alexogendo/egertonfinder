package org.apache.jsp.manager;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.time.*;
import java.text.*;
import java.time.format.*;
import admins.ConnectionManager;

public final class mregisterpcs_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write('\n');
      out.write('\n');

    String ids = (String)session.getAttribute("id");
    String fnames = (String)session.getAttribute("fname");
    String lnames  = (String)session.getAttribute("lname");
    int adminid = Integer.parseInt(ids);
    
        String adminnames = fnames + " "+ lnames;
        
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
	Date date = new Date();
        String regdate = dateFormat.format(date);
        
        DateFormat dateFormat2 = new SimpleDateFormat("HH:mm:ss");
	Date date2 = new Date();
        String regtime = dateFormat2.format(date2);
    
      out.write('\n');
      out.write('\n');
            
              
//    FileInputStream fis=null;
    PreparedStatement pstmt = null;
    ResultSet rs=null;
    Connection conn=null;
                        String mac = request.getParameter("serial");
                        String mail = request.getParameter("email");
                        String fname = request.getParameter("firstname");
                        String lname = request.getParameter("lastname");
                        String phon = request.getParameter("phone");
                        String nId = request.getParameter("id");
                        String categ = request.getParameter("category");
                        String desc = request.getParameter("description");
                       // String myloc=request.getParameter("userimg");
            
    try
        {
            ConnectionManager manager= new ConnectionManager();  
             conn = manager.getConnect();    
            
            Statement st1=conn.createStatement();
             String strQuery = "SELECT COUNT(*) FROM recorderdpcs where macadress='"+mac+"'";
                        rs = st1.executeQuery(strQuery);
                        rs.next();
                        String Countrow = rs.getString(1);
                        System.out.println(Countrow);
                        if(Countrow.equals("0"))
                        {
            //Create sql statement and insert the security administrator
                           // File image= new File(myloc);
                     // Statement st=conn.createStatement();
                      pstmt = conn.prepareStatement("INSERT INTO recorderdpcs (type, macadress, description, adminname, adminId, ownerFname, ownerLname, ownerid, ownerphone, owneremail, dateregistered, timeregistered) " + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?)");
                     // int i=st.executeUpdate("INSERT INTO admins (firstname,lastname, password, email, phone, idNo, role, department) VALUES ('"+fname+"','"+lname+"','"+pass+"','"+mail+"','"+phon+"','"+nId+"','"+rol+"','"+depart+"')");
                        pstmt.setString(1, categ);
                        pstmt.setString(2, mac);
                        pstmt.setString(3, desc);
                        
                        pstmt.setString(4, adminnames);
                        pstmt.setInt(5, adminid);
                        
                        pstmt.setString(6, fname);
                        pstmt.setString(7, lname);
                        pstmt.setString(8, nId);
                        pstmt.setString(9, phon);
                        pstmt.setString(10, mail);
                        
                        pstmt.setString(11, regdate);
                        pstmt.setString(12, regtime);
//                        fis=new FileInputStream(image);
//                        pstmt.setBinaryStream(9, (InputStream) fis, (int) (image.length()));
                        int count = pstmt.executeUpdate();
                        if(count>0)
                            {
                     
                      System.out.println("Data was successfully inserted!");
                      
                      String redirectURL = "http://localhost:8080/egertonfinder/manager/registerpcsuccess.jsp?Device has been added successfully";
                      response.sendRedirect(redirectURL);
                      } 
                        else{
                            System.out.println("Data insertion failed!");
                      
                      String redirectURL1 = "http://localhost:8080/egertonfinder/errorpage.jsp? We have noticed ubnormal behavior!";
                      response.sendRedirect(redirectURL1);
                        }
                        }//End of if statement
                        else{
                        System.out.println("The national id number or Email already exists !");
                        
                        String redirectURL2 = "http://localhost:8080/egertonfinder/manager/regsterpcfail.jsp?Device has been already registered";
                        response.sendRedirect(redirectURL2);
                        }

                        }
                        catch(Exception e)
                        {
                        System.out.print(e);
                        e.printStackTrace();
                        }
                                finally{
                            try{
                            if(rs!=null){
                            rs.close();
                            rs= null;
                            }
                            if(pstmt !=null)
                            {
                            pstmt.close();
                            pstmt=null;
                            }
                            if(conn!=null)
                            {
                            conn.close();
                            conn=null;
                            }
                            }
                            catch(Exception e)
                            {
                            e.printStackTrace();
                            }
                            }
    
        
      out.write('\n');
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
