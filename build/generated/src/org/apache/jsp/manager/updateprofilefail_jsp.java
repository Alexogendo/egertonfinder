package org.apache.jsp.manager;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Connection;
import admins.ConnectionManager;

public final class updateprofilefail_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");

    String ids = (String)session.getAttribute("id");
    String fnames = (String)session.getAttribute("fname");
    String lnames  = (String)session.getAttribute("lname");
    String mails = (String)session.getAttribute("mail");
    String phones = (String)session.getAttribute("phone");
    String nationals  = (String)session.getAttribute("national");
    String roles = (String)session.getAttribute("role");
    
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html lang=\"en\">\n");
      out.write("\n");
      out.write("<head>\n");
      out.write("  <meta charset=\"utf-8\" />\n");
      out.write("  <link rel=\"apple-touch-icon\" sizes=\"76x76\" href=\"../content/images/img_467397.png\">\n");
      out.write("    <link rel=\"icon\" type=\"image/png\" sizes=\"96x96\" href=\"../content/images/img_467397.png\">\n");
      out.write("    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\" />\n");
      out.write("  <title>\n");
      out.write("    Egerton Lost and Lost Found Systems\n");
      out.write("  </title>\n");
      out.write("  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />\n");
      out.write("  <!--     Fonts and icons     -->\n");
      out.write("  <link href=\"https://fonts.googleapis.com/css?family=Montserrat:400,700,200\" rel=\"stylesheet\" />\n");
      out.write("  <link href=\"https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css\" rel=\"stylesheet\">\n");
      out.write("  <!-- CSS Files -->\n");
      out.write("  <link href=\"../assets/css/bootstrap.min.css\" rel=\"stylesheet\" />\n");
      out.write("  <link href=\"../assets/css/paper-dashboard.css?v=2.0.0\" rel=\"stylesheet\" />\n");
      out.write("  <!-- CSS Just for demo purpose, don't include it in your project -->\n");
      out.write("  <link href=\"../assets/demo/demo.css\" rel=\"stylesheet\" />\n");
      out.write("  <style>\n");
      out.write("     #onhoverLogout{\n");
      out.write("                visibility: hidden;\n");
      out.write("            }\n");
      out.write("            #onlogoutIcon:hover ~ #onhoverLogout{\n");
      out.write("                visibility: visible;\n");
      out.write("            }\n");
      out.write("    </style>\n");
      out.write("</head>\n");
      out.write("\n");
      out.write("<body class=\"\" onload=\"demo.showNotification27('top','center')\">\n");
      out.write("  <div class=\"wrapper \">\n");
      out.write("    <div class=\"sidebar\" data-color=\"green\" data-active-color=\"danger\">\n");
      out.write("      <!--\n");
      out.write("        Tip 1: You can change the color of the sidebar using: data-color=\"blue | green | orange | red | yellow\"\n");
      out.write("    -->\n");
      out.write("      <div class=\"logo\">\n");
      out.write("        <a href=\"http://www.creative-tim.com\" class=\"simple-text logo-mini\">\n");
      out.write("          <div class=\"logo-image-small\">\n");
      out.write("            <img src=\"../content/images/img_467397.png\">\n");
      out.write("          </div>\n");
      out.write("        </a>\n");
      out.write("        <a href=\"http://www.creative-tim.com\" class=\"simple-text logo-normal\">\n");
      out.write("          Efinder\n");
      out.write("          <!-- <div class=\"logo-image-big\">\n");
      out.write("            <img src=\"../assets/img/logo-big.png\">\n");
      out.write("          </div> -->\n");
      out.write("        </a>\n");
      out.write("      </div>\n");
      out.write("      <div class=\"sidebar-wrapper\">\n");
      out.write("       <ul class=\"nav\" style=\"\">\n");
      out.write("          <li class=\"hove\">\n");
      out.write("            <a href=\"./dashboard.jsp\">\n");
      out.write("              <i class=\"nc-icon nc-bank\"></i>\n");
      out.write("              <p>Dashboard</p>\n");
      out.write("            </a>\n");
      out.write("          </li>\n");
      out.write("          \n");
      out.write("          <li class=\"hove\">\n");
      out.write("            <a href=\"./postfounditem.jsp\">\n");
      out.write("              <i class=\"nc-icon nc-sound-wave\"></i>\n");
      out.write("              <p>Post Found Item</p>\n");
      out.write("            </a>\n");
      out.write("          </li>\n");
      out.write("          <li class=\"hove\">\n");
      out.write("            <a href=\"./postlostitem.jsp\">\n");
      out.write("              <i class=\"nc-icon nc-sun-fog-29\"></i>\n");
      out.write("              <p>Post Lost Item</p>\n");
      out.write("            </a>\n");
      out.write("          </li>\n");
      out.write("          <li class=\"hove\">\n");
      out.write("            <a href=\"./assignitems.jsp\">\n");
      out.write("              <i class=\"nc-icon nc-bulb-63\"></i>\n");
      out.write("              <p>Assign Items to Owners</p>\n");
      out.write("            </a>\n");
      out.write("          </li>\n");
      out.write("          <li class=\"hove\">\n");
      out.write("            <a href=\"./entries.jsp\">\n");
      out.write("              <i class=\"nc-icon nc-tile-56\"></i>\n");
      out.write("              <p>System Entries</p>\n");
      out.write("            </a>\n");
      out.write("          </li>\n");
      out.write("          <li class=\"hove\">\n");
      out.write("            <a href=\"./printreports.jsp\">\n");
      out.write("              <i class=\"nc-icon nc-paper\"></i>\n");
      out.write("              <p>Print Reports</p>\n");
      out.write("            </a>\n");
      out.write("          </li>\n");
      out.write("          <li class=\"hove\">\n");
      out.write("            <a href=\"./registeradmin.jsp\">\n");
      out.write("              <i class=\"nc-icon nc-bullet-list-67\"></i>\n");
      out.write("              <p>Register Admin</p>\n");
      out.write("            </a>\n");
      out.write("          </li>\n");
      out.write("          <li class=\"hove\">\n");
      out.write("            <a href=\"./registerclients&pcs.jsp\">\n");
      out.write("              <i class=\"nc-icon nc-single-copy-04\"></i>\n");
      out.write("              <p>Register Clients & Pc's</p>\n");
      out.write("            </a>\n");
      out.write("          </li>\n");
      out.write("          <li class=\"hove\">\n");
      out.write("            <a href=\"./user.jsp\">\n");
      out.write("              <i class=\"nc-icon nc-single-02\"></i>\n");
      out.write("              <p>User Profile</p>\n");
      out.write("            </a>\n");
      out.write("          </li>\n");
      out.write("          \n");
      out.write("          <li class=\"hove\">\n");
      out.write("            <a href=\"http://localhost:8080/egertonfinder/Help/index.html\">\n");
      out.write("              <i class=\"nc-icon nc-spaceship\"></i>\n");
      out.write("              <p>Gain Experience</p>\n");
      out.write("            </a>\n");
      out.write("          </li>\n");
      out.write("        </ul>\n");
      out.write("      </div>\n");
      out.write("      </div>\n");
      out.write("    </div>\n");
      out.write("    <div class=\"main-panel\" style=\"margin-top: -52%\">\n");
      out.write("      <!-- Navbar -->\n");
      out.write("      <nav class=\"navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent\">\n");
      out.write("        <div class=\"container-fluid\">\n");
      out.write("          <div class=\"navbar-wrapper\">\n");
      out.write("            <div class=\"navbar-toggle\">\n");
      out.write("              <button type=\"button\" class=\"navbar-toggler\">\n");
      out.write("                <span class=\"navbar-toggler-bar bar1\"></span>\n");
      out.write("                <span class=\"navbar-toggler-bar bar2\"></span>\n");
      out.write("                <span class=\"navbar-toggler-bar bar3\"></span>\n");
      out.write("              </button>\n");
      out.write("            </div>\n");
      out.write("            <a class=\"navbar-brand\" href=\"#\">Egerton Lost and Found Systems</a>\n");
      out.write("          </div>\n");
      out.write("          <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navigation\" aria-controls=\"navigation-index\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n");
      out.write("            <span class=\"navbar-toggler-bar navbar-kebab\"></span>\n");
      out.write("            <span class=\"navbar-toggler-bar navbar-kebab\"></span>\n");
      out.write("            <span class=\"navbar-toggler-bar navbar-kebab\"></span>\n");
      out.write("          </button>\n");
      out.write("          <div class=\"collapse navbar-collapse justify-content-end\" id=\"navigation\">\n");
      out.write("            <form>\n");
      out.write("              <div class=\"input-group no-border\">\n");
      out.write("                <input type=\"text\" value=\"\" class=\"form-control\" placeholder=\"Search...\">\n");
      out.write("                <div class=\"input-group-append\">\n");
      out.write("                  <div class=\"input-group-text\">\n");
      out.write("                    <i class=\"nc-icon nc-zoom-split\"></i>\n");
      out.write("                  </div>\n");
      out.write("                </div>\n");
      out.write("              </div>\n");
      out.write("            </form>\n");
      out.write("           <ul class=\"navbar-nav\">\n");
      out.write("<!--              <li class=\"nav-item\">\n");
      out.write("                <a class=\"nav-link btn-magnify\" href=\"#pablo\">\n");
      out.write("                  <i class=\"nc-icon nc-layout-11\"></i>\n");
      out.write("                  <p>\n");
      out.write("                    <span class=\"d-lg-none d-md-block\">Stats</span>\n");
      out.write("                  </p>\n");
      out.write("                </a>\n");
      out.write("              </li>-->\n");
      out.write("              <li class=\"nav-item btn-rotate dropdown\">\n");
      out.write("                <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n");
      out.write("                  <i class=\"nc-icon nc-bell-55\"></i>\n");
      out.write("                  <p>\n");
      out.write("                    <span class=\"d-lg-none d-md-block\">Some Actions</span>\n");
      out.write("                  </p>\n");
      out.write("                </a>\n");
      out.write("                <div class=\"dropdown-menu dropdown-menu-right\" aria-labelledby=\"navbarDropdownMenuLink\">\n");
      out.write("                 <a class=\"dropdown-item\" href=\"./notifications.jsp\">Notifications</a>\n");
      out.write("                  <a class=\"dropdown-item\" href=\"./user.jsp\">Your Profile</a>\n");
      out.write("                  <a class=\"dropdown-item\" href=\"./changepassword.jsp\">Change Password</a>\n");
      out.write("                </div>\n");
      out.write("              </li>\n");
      out.write("              <li class=\"nav-item\">\n");
      out.write("                <a class=\"nav-link btn-rotate\" href=\"../logout\">\n");
      out.write("                    <div id=\"onlogoutIcon\">\n");
      out.write("                  <i class=\"nc-icon nc-button-power\"></i>\n");
      out.write("                    </div>\n");
      out.write("                  <div id=\"onhoverLogout\">Logout</div>\n");
      out.write("                  \n");
      out.write("                </a>\n");
      out.write("              </li>\n");
      out.write("            </ul>\n");
      out.write("          </div>\n");
      out.write("        </div>\n");
      out.write("      </nav>\n");
      out.write("      <!-- End Navbar -->\n");
      out.write("      <!-- <div class=\"panel-header panel-header-sm\">\n");
      out.write("  \n");
      out.write("  \n");
      out.write("</div> -->\n");
      out.write("      ");

                        
                        Connection conn=null;
                        Statement statement = null;
                        ResultSet resultSet = null;
                           int newclientid = Integer.parseInt(ids); 
                        
      out.write("\n");
      out.write("                        \n");
      out.write("                        ");

                                       
                                try{
                                     ConnectionManager manager= new ConnectionManager();  
                                     conn = manager.getConnect();
                                statement=conn.createStatement();
                                String sql ="select * from users WHERE userId='"+newclientid+"'";
                                resultSet = statement.executeQuery(sql);
                                while(resultSet.next()){
                                    String F1= resultSet.getString("firstname");
                                    String F2= resultSet.getString("lastname");
                                    String Fmail= resultSet.getString("email");
                                    String Fphone= resultSet.getString("phone");
                                    String Fid= resultSet.getString("idNo");
                                    String Frole= resultSet.getString("role");
                                    String Fdepart= resultSet.getString("department");
                                
                         
      out.write("       \n");
      out.write("                        \n");
      out.write("      <div class=\"content\">\n");
      out.write("        <div class=\"row\">\n");
      out.write("          <div class=\"col-md-4\">\n");
      out.write("            <div class=\"card card-user\">\n");
      out.write("              <div class=\"image\">\n");
      out.write("                <img src=\"\" alt=\"\">\n");
      out.write("              </div>\n");
      out.write("              <div class=\"card-body\">\n");
      out.write("                <div class=\"author\">\n");
      out.write("                  <a href=\"#\">\n");
      out.write("                    <img class=\"avatar border-gray\" src=\"../assets/img/icons8-user-80.png\" alt=\"...\">\n");
      out.write("                    <h5 class=\"title\">");
out.println(" "+F1 +" "+F2);
      out.write("</h5>\n");
      out.write("                  </a>\n");
      out.write("                  <p class=\"descripti\" style=\"text-align: left; font-size: 15px\"> \n");
      out.write("                      <strong>User ID:</strong> ");
out.println(" "+ids);
      out.write(" <br>\n");
      out.write("                      <strong>Role:</strong> ");
out.println(" "+Frole);
      out.write(" <br>\n");
      out.write("                      <strong>Department:</strong> ");
out.println(" "+Fdepart);
      out.write("<br>\n");
      out.write("                      <strong>Email:</strong> ");
out.println(" "+Fmail);
      out.write(" <br>\n");
      out.write("                      <strong>Phone:</strong> ");
out.println(" "+Fphone);
      out.write(" <br>\n");
      out.write("                      <strong>National ID:</strong> ");
out.println(" "+Fid);
      out.write("<br>\n");
      out.write("                      \n");
      out.write("                  </p>\n");
      out.write("                </div>\n");
      out.write("                <p class=\"description text-center\">\n");
      out.write("                  \n");
      out.write("                  \n");
      out.write("                </p>\n");
      out.write("              </div>\n");
      out.write("              <div class=\"card-footer\">\n");
      out.write("                <hr>\n");
      out.write("                <div class=\"button-container\">\n");
      out.write("                  <div class=\"row\">\n");
      out.write("                    <div class=\"col-lg-3 col-md-6 col-6 ml-auto\">\n");
      out.write("                      <h5>\n");
      out.write("                        <br>\n");
      out.write("                        <small></small>\n");
      out.write("                      </h5>\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"col-lg-4 col-md-6 col-6 ml-auto mr-auto\">\n");
      out.write("                      <h5>\n");
      out.write("                        <br>\n");
      out.write("                        <small></small>\n");
      out.write("                      </h5>\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"col-lg-3 mr-auto\">\n");
      out.write("                      <h5>\n");
      out.write("                        <br>\n");
      out.write("                        <small></small>\n");
      out.write("                      </h5>\n");
      out.write("                    </div>\n");
      out.write("                  </div>\n");
      out.write("                </div>\n");
      out.write("              </div>\n");
      out.write("            </div>\n");
      out.write("            \n");
      out.write("          </div>\n");
      out.write("          <div class=\"col-md-8\">\n");
      out.write("            <div class=\"card card-user\">\n");
      out.write("              <div class=\"card-header\">\n");
      out.write("                <h5 class=\"card-title\">Update Profile</h5>\n");
      out.write("                <p><i>Dear ");
out.println(F1);
      out.write(",</i> note that you are only allowed to edit phone and email fields <strong style=\"color: red\">ONLY.</strong></p>\n");
      out.write("              </div>\n");
      out.write("              <div class=\"card-body\">\n");
      out.write("                  <form method=\"post\" action=\"updateprofile.jsp\">\n");
      out.write("                  <div class=\"row\">\n");
      out.write("                    <div class=\"col-md-5 pr-1\">\n");
      out.write("                      <div class=\"form-group\">\n");
      out.write("                        <label>Account Name (disabled)</label>\n");
      out.write("                        <input type=\"text\" class=\"form-control\" disabled=\"\" placeholder=\"Company\" value=\"Efinder Systems\">\n");
      out.write("                      </div>\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"col-md-3 px-1\">\n");
      out.write("                      <div class=\"form-group\">\n");
      out.write("                        <label>Role</label>\n");
      out.write("                        <input type=\"text\" class=\"form-control\" value=\"");
out.println(Frole);
      out.write("\" readonly>\n");
      out.write("                      </div>\n");
      out.write("                    </div>\n");
      out.write("\n");
      out.write("                  </div>\n");
      out.write("                  <div class=\"row\">\n");
      out.write("                    <div class=\"col-md-6 pr-1\">\n");
      out.write("                      <div class=\"form-group\">\n");
      out.write("                        <label>First Name</label>\n");
      out.write("                        <input type=\"text\" class=\"form-control\" value=\"");
out.println(F1);
      out.write("\" readonly>\n");
      out.write("                      </div>\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"col-md-6 pl-1\">\n");
      out.write("                      <div class=\"form-group\">\n");
      out.write("                        <label>Last Name</label>\n");
      out.write("                        <input type=\"text\" class=\"form-control\" value=\"");
out.println(F2);
      out.write("\" minlength=\"4\" maxlenth=\"30\" readonly>\n");
      out.write("                      </div>\n");
      out.write("                    </div>\n");
      out.write("                  </div>\n");
      out.write("                  <div class=\"row\">\n");
      out.write("                    <div class=\"col-md-6 pr-1\">\n");
      out.write("                      <div class=\"form-group\">\n");
      out.write("                        <label>Phone Number</label>\n");
      out.write("                        <input type=\"text\" class=\"form-control\" name=\"phone\" value=\"");
out.println(Fphone);
      out.write("\" minlength=\"10\" maxlength=\"10\" autocomplete=\"off\" required=\"required\">\n");
      out.write("                      </div>\n");
      out.write("                    </div>\n");
      out.write("                      <div class=\"col-md-6 pl-1\">\n");
      out.write("                      <div class=\"form-group\">\n");
      out.write("                        <label for=\"exampleInputEmail1\">Email address</label>\n");
      out.write("                        <input type=\"email\" class=\"form-control\" name=\"mail\" value=\"");
out.println(Fmail);
      out.write("\" autocomplete=\"off\" minlength=\"11\" maxlength=\"100\" required=\"required\">\n");
      out.write("                      </div>\n");
      out.write("                    </div>\n");
      out.write("                      \n");
      out.write("                  </div>\n");
      out.write("                    <div class=\"row\">\n");
      out.write("                    <div class=\"col-md-12\">\n");
      out.write("                      <div class=\"form-group\">\n");
      out.write("                        <label>National ID</label>\n");
      out.write("                        <input type=\"text\" class=\"form-control\" value=\"");
out.println(Fid);
      out.write("\" minlength=\"12\" maxlength=\"12\" readonly>\n");
      out.write("                      </div>\n");
      out.write("                    </div>\n");
      out.write("                  </div>\n");
      out.write("                  <div class=\"row\">\n");
      out.write("                    <div class=\"update ml-auto mr-auto\">\n");
      out.write("                      <button type=\"submit\" class=\"btn btn-primary btn-round\">Update Profile</button>\n");
      out.write("                    </div>\n");
      out.write("                  </div>\n");
      out.write("                </form>\n");
      out.write("              </div>\n");
      out.write("            </div>\n");
      out.write("          </div>\n");
      out.write("        </div>\n");
      out.write("      </div>\n");
      out.write("                      ");

                   
                   session.setAttribute("Fmail", Fmail);
                   session.setAttribute("Fphone", Fphone);
                   
                                }
                                conn.close();
                                } catch (Exception e) {
                                e.printStackTrace();
                                }

      out.write("\n");
      out.write("      <footer class=\"footer footer-black  footer-white \">\n");
      out.write("        <div class=\"container-fluid\">\n");
      out.write("          <div class=\"row\">\n");
      out.write("            <nav class=\"footer-nav\">\n");
      out.write("              <ul>\n");
      out.write("                <li>\n");
      out.write("                  <a href=\"#\" target=\"_blank\">Efinder</a>\n");
      out.write("                </li>\n");
      out.write("                <li>\n");
      out.write("                  <a href=\"#\" target=\"_blank\">Blog</a>\n");
      out.write("                </li>\n");
      out.write("                <li>\n");
      out.write("                  <a href=\"#\" target=\"_blank\">Licenses</a>\n");
      out.write("                </li>\n");
      out.write("              </ul>\n");
      out.write("            </nav>\n");
      out.write("            <div class=\"credits ml-auto\">\n");
      out.write("              <span class=\"copyright\">\n");
      out.write("                ©\n");
      out.write("                <script>\n");
      out.write("                  document.write(new Date().getFullYear())\n");
      out.write("                </script>, made with <i class=\"fa fa-heart heart\"></i> by Efinder Developers\n");
      out.write("              </span>\n");
      out.write("            </div>\n");
      out.write("          </div>\n");
      out.write("        </div>\n");
      out.write("      </footer>\n");
      out.write("    </div>\n");
      out.write("  </div>\n");
      out.write("  <!--   Core JS Files   -->\n");
      out.write("  <script src=\"../assets/js/core/jquery.min.js\"></script>\n");
      out.write("  <script src=\"../assets/js/core/popper.min.js\"></script>\n");
      out.write("  <script src=\"../assets/js/core/bootstrap.min.js\"></script>\n");
      out.write("  <script src=\"../assets/js/plugins/perfect-scrollbar.jquery.min.js\"></script>\n");
      out.write("  <!--  Google Maps Plugin    -->\n");
      out.write("  <script src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyD-EqDa-aTpuUS0d9A6VCx8pH-kM6C3Q2k\"></script>\n");
      out.write("  <!-- Chart JS -->\n");
      out.write("  <script src=\"../assets/js/plugins/chartjs.min.js\"></script>\n");
      out.write("  <!--  Notifications Plugin    -->\n");
      out.write("  <script src=\"../assets/js/plugins/bootstrap-notify.js\"></script>\n");
      out.write("  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->\n");
      out.write("  <script src=\"../assets/js/paper-dashboard.min.js?v=2.0.0\" type=\"text/javascript\"></script>\n");
      out.write("  <!-- Paper Dashboard DEMO methods, don't include it in your project! -->\n");
      out.write("  <script src=\"../assets/demo/demo.js\"></script>\n");
      out.write("</body>\n");
      out.write("\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
