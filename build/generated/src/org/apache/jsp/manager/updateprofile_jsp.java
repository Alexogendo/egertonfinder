package org.apache.jsp.manager;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.*;
import java.sql.Connection;
import admins.ConnectionManager;

public final class updateprofile_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");

           
            
    String ids = (String)session.getAttribute("id");
    String fnames = (String)session.getAttribute("fname");
    String lnames  = (String)session.getAttribute("lname");
    String mails = (String)session.getAttribute("Fmail");
    String phones = (String)session.getAttribute("Fphone");
    String nationals  = (String)session.getAttribute("national");
    String roles = (String)session.getAttribute("role");
    

                String newphone = request.getParameter("phone");
                String newemail = request.getParameter("mail");


      out.write('\n');
      out.write('\n');

                        
                        if(ids != null)
                        {
                            
                        Connection conn= null;
                        PreparedStatement ps = null;
                        Statement st = null;
                        ResultSet rs = null;
                        int newid = Integer.parseInt(ids);
                        try
                        {
                            ConnectionManager manager= new ConnectionManager();  
                             conn = manager.getConnect();
                             
                               
                               if(newemail.equals(mails) && newphone.equals(phones)){
                                   
                                   String sql1="Update users set email=?, phone=? where userId='"+ ids +"'";
                                    ps = conn.prepareStatement(sql1);
                                    ps.setString(1,newemail);
                                    ps.setString(2,newphone);
                                    
                                    int i = ps.executeUpdate();
                                    if(i > 0)
                                    {
                                        String redirectURL4 = "http://localhost:8080/egertonfinder/manager/updateprofilesuccess.jsp?You have updated your profile successfully";
                                        
                                        response.sendRedirect(redirectURL4);
                                    } 
              
                                     else
                                    {
                                   // out.print("There is a problem in updating Record.");
                                    String redirectURL5 = "http://localhost:8080/egertonfinder/manager/updateprofilefail.jsp?Updating profile failed. Check your details and try again!";
                                    response.sendRedirect(redirectURL5);
                                    }
                                    }
                               
                                    else if(!newemail.equals(mails) && newphone.equals(phones)){
                                    st=conn.createStatement();
                                    String strQuery = "SELECT COUNT(*) FROM users where email='"+newemail+"'";
                                     rs = st.executeQuery(strQuery);
                                     rs.next();
                                     String Countrow = rs.getString(1);
                                     System.out.println(Countrow);
                                
                                            if(Countrow.equals("0"))
                                            {
                                                String sql="Update users set email=?, phone=? where userId='"+ ids +"'";
                                    ps = conn.prepareStatement(sql);
                                    ps.setString(1,newemail);
                                    ps.setString(2,newphone);
                                    
                                    int i = ps.executeUpdate();
                                    if(i > 0)
                                    {
                                        String redirectURL = "http://localhost:8080/egertonfinder/manager/updateprofilesuccess.jsp?You have updated your profile successfully";
                                        
                                        response.sendRedirect(redirectURL);
                                    } 
              
                                     else
                                    {
                                   // out.print("There is a problem in updating Record.");
                                    String redirectURL2 = "http://localhost:8080/egertonfinder/manager/updateprofilefail.jsp?Updating profile failed. Check your details and try again!";
                                    response.sendRedirect(redirectURL2);
                                    }
                                    }
                                    else
                                    {
                                   // out.print("There is a problem in updating Record.");
                                    String redirectURL2 = "http://localhost:8080/egertonfinder/manager/updateprofilefail.jsp?Updating profile failed. Check your details and try again!";
                                    response.sendRedirect(redirectURL2);
                                    }
                                    }
                                    
                                    else if(newemail.equals(mails) && !newphone.equals(phones))
                                    {              
                                        st=conn.createStatement();
                                        String strQuery2 = "SELECT COUNT(*) FROM users where phone='"+newphone+"'";
                                        rs = st.executeQuery(strQuery2);
                                        rs.next();
                                        String Countrow2 = rs.getString(1);
                                        System.out.println(Countrow2);
                                
                                            if(Countrow2.equals("0")){
                                                    String sql="Update users set email=?, phone=? where userId='"+ ids +"'";
                                                    ps = conn.prepareStatement(sql);
                                                    ps.setString(1,newemail);
                                                    ps.setString(2,newphone);
                                    
                                            int i = ps.executeUpdate();
                                            if(i > 0)
                                                {
                                                    String redirectURL11 = "http://localhost:8080/egertonfinder/manager/updateprofilesuccess.jsp?You have updated your profile successfully";

                                                    response.sendRedirect(redirectURL11);
                                                } 
              
                                     else
                                    {
                                   // out.print("There is a problem in updating Record.");
                                    String redirectURL12 = "http://localhost:8080/egertonfinder/manager/updateprofilefail.jsp?Updating profile failed. Check your details and try again!";
                                    response.sendRedirect(redirectURL12);
                                    }
                                    } 
                                     else
                                    {
                                   // out.print("There is a problem in updating Record.");
                                    String redirectURL20 = "http://localhost:8080/egertonfinder/manager/updateprofilefail.jsp?Updating profile failed. Check your details and try again!";
                                    response.sendRedirect(redirectURL20);
                                     }
                                    
                                    }
                                            
                                    else{
                                    st=conn.createStatement();
                                    String strQuery11 = "SELECT COUNT(*) FROM users where email='"+newemail+"'";
                                     rs = st.executeQuery(strQuery11);
                                     rs.next();
                                     String Countrow31 = rs.getString(1);
                                     System.out.println(Countrow31);
                                
                                            if(Countrow31.equals("0"))
                                            {
                                                st=conn.createStatement();
                                        String strQuery21 = "SELECT COUNT(*) FROM users where phone='"+newphone+"'";
                                        rs = st.executeQuery(strQuery21);
                                        rs.next();
                                        String Countrow21 = rs.getString(1);
                                        System.out.println(Countrow21);
                                
                                            if(Countrow21.equals("0")){
                                                    String sql20="Update users set email=?, phone=? where userId='"+ ids +"'";
                                                    ps = conn.prepareStatement(sql20);
                                                    ps.setString(1,newemail);
                                                    ps.setString(2,newphone);
                                    
                                            int i2 = ps.executeUpdate();
                                            if(i2 > 0)
                                                {
                                                    String redirectURL11 = "http://localhost:8080/egertonfinder/manager/updateprofilesuccess.jsp?You have updated your profile successfully";

                                                    response.sendRedirect(redirectURL11);
                                                } 
              
                                     else
                                    {
                                   // out.print("There is a problem in updating Record.");
                                    String redirectURL12 = "http://localhost:8080/egertonfinder/manager/updateprofilefail.jsp?Updating profile failed. Check your details and try again!";
                                    response.sendRedirect(redirectURL12);
                                    }
                                            } 
                                            else
                                    {
                                   // out.print("There is a problem in updating Record.");
                                    String redirectURL122 = "http://localhost:8080/egertonfinder/manager/updateprofilefail.jsp?Updating profile failed. Check your details and try again!";
                                    response.sendRedirect(redirectURL122);
                                    }
                                            }    
                                              else
                                    {
                                   // out.print("There is a problem in updating Record.");
                                    String redirectURL122 = "http://localhost:8080/egertonfinder/manager/updateprofilefail.jsp?Updating profile failed. Check your details and try again!";
                                    response.sendRedirect(redirectURL122);
                                    }
                                            
                                     }
                               
                                    
                                     }
                        
                                    catch(SQLException sql)
                                            {
                                    request.setAttribute("error", sql);
                                    out.println(sql);
                                            }
                                        }
                                        else{
                            // out.print("There is a problem in updating Record.");
                                    String redirectURL23 = "http://localhost:8080/egertonfinder/manager/users.jsp?Manager id is unknown";
                                    response.sendRedirect(redirectURL23);
                        }
                                    
      out.write("            ");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
