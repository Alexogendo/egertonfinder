package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			"errorpage.jsp", true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html lang=\"en\">\n");
      out.write("<head>\n");
      out.write("\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n");
      out.write("\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no\">\n");
      out.write("\t<meta name=\"google\" content=\"notranslate\"/>\n");
      out.write("\t<meta name=\"description\" content=\"Efinder Login | Sign into your Efinder account.\" />\n");
      out.write("\t<meta name=\"robots\" content=\"index, follow\" />\n");
      out.write("\n");
      out.write("\t<title>Efinder Login</title>\n");
      out.write("\n");
      out.write("\t<link rel=\"stylesheet\" href=\"content/css/fonts.min.css\" />\n");
      out.write("\t<link rel=\"stylesheet\" href=\"content/css/min.css\" />\n");
      out.write("\t<link rel=\"stylesheet\" href=\"content/css/login.min.css\" />\n");
      out.write("\t<script src=\"content/js/polyfills.min.js\"></script>\n");
      out.write("\t<script src=\"content/js/core-public.min.js\"></script>\n");
      out.write("\t<script src=\"content/js/mask.min.js\"></script>\n");
      out.write("\t<script src=\"content/js/login.min.js\"></script>\n");
      out.write("\n");
      out.write("\t<link rel=\"shortcut icon\" href=\"content/images/img_467397.png\" />\n");
      out.write("        \n");
      out.write("<script> \n");
      out.write("function validate()\n");
      out.write("{ \n");
      out.write(" var Email = document.form.Email.value;\n");
      out.write(" var Password = document.form.Password.value;\n");
      out.write("  \n");
      out.write(" if (Email===null || Email===\"\")\n");
      out.write(" { \n");
      out.write(" alert(\"Email field cannot be blank\"); \n");
      out.write(" return false; \n");
      out.write(" }\n");
      out.write(" else if (Password===null || Password===\"\")\n");
      out.write(" { \n");
      out.write(" alert(\"Password field cannot be blank\"); \n");
      out.write(" return false; \n");
      out.write(" }\n");
      out.write("  else if(Password.length<6)\n");
      out.write(" { \n");
      out.write(" alert(\"Password must be at least 6 characters long.\"); \n");
      out.write(" return false; \n");
      out.write(" } \n");
      out.write(" } \n");
      out.write("</script> \n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("\t<script id=\"CaspioConfig\" type=\"text/json\">\n");
      out.write("            {\"caspioDomain\":null,\"releaseVersion\":\"17.##VersionPostfix##\",\"forgotPwdURL\":\"https://id.caspio.com/forgotpassword\",\"forgotLoginUrl\":\"\",\"supportURL\":\"https://www.caspio.com/support/\",\"gettingStartedURL\":\"https://www.caspio.com/get-started/\",\"freeTrialUrl\":\"https://pages.caspio.com/free-trial\",\"privacyURL\":\"https://www.caspio.com/caspio-privacy-statement/\",\"termsOfUse\":\"https://www.caspio.com/terms-of-use-agreement/\",\"termsOfServiceUrl\":\"https://www.caspio.com/caspio-bridge-terms-of-service/\",\"systemRequirementsUrl\":\"https://howto.caspio.com/system-requirements/\",\"imagePath\":\"/content/images\",\"passwordSecurityURL\":null,\"changePasswordURL\":null,\"validatePasswordURL\":null,\"myProfileURL\":null,\"partnersPortalMenuLinkVisible\":false,\"partnersPortalMenuLinkUrl\":null,\"loginURL\":null,\"cloudLogOutURL\":null,\"dashboardURL\":null,\"signupURL\":null,\"signupProfileURL\":null,\"minLengthCustomSubdomainName\":0,\"maxLengthCustomSubdomainName\":0,\"passwordAjaxValidationDelay\":0,\"sessionExpiredURL\":null,\"logoutUrl\":null,\"sessionExpirationCountdownSeconds\":0,\"sessionExpirationSimplePingSeconds\":0,\"openSessionExpirationDialogRePingFrequency\":0,\"allow2FA\":false,\"showSendAgainTfaOtpTimeout\":60,\"tfaSmsAllowedCountries\":null}\n");
      out.write("</script>\n");
      out.write("<script id=\"CaspioRequest\" type=\"text/json\">\n");
      out.write("\t{\"UserName\":null,\"Email\":null}\n");
      out.write("</script>\n");
      out.write("\n");
      out.write("<div class=\"cb_container\">\n");
      out.write("\t<div class=\"cb_body\">\n");
      out.write("\t\t<div class=\"left-side\">\n");
      out.write("\n");
      out.write("\t\t\t<div id=\"TopAnnouncement\" class=\"top-announcement\"></div>\n");
      out.write("\n");
      out.write("\t\t\t<div class=\"container-info\">\n");
      out.write("\t\t\t\t\n");
      out.write("<link rel=\"stylesheet\" href=\"/content/less/caspio-logo.min.css\" />\n");
      out.write("\n");
      out.write("<div class=\"caspio-logo\">\n");
      out.write("    <img src=\"content/images/img_467397.png\" height=\"50\" width=\"50\" style=\"margin-left: 240px\" alt=\"\" /><span ><h3 style=\"margin-top: -40px; margin-left: 150px\">Efinder</h3></span>\n");
      out.write("</div>\n");
      out.write("<br>\n");
      out.write("\t\t\t\t<div id=\"first-login-step\" class=\"box\">\n");
      out.write("                                    <form  name=\"form\" autocomplete=\"off\" action=\"login\" onsubmit=\"return validate()\">\n");
      out.write("\t\t\t\t\t\t<noscript>\n");
      out.write("\t\t\t\t\t\t\t<div class=\"validation-message-error\">\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"icon\"></div>\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"message\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<b>JavaScript must be enabled.</b>Please enable JavaScript in your browser to use Efinder System.\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t<div class=\"form-row need-help\">\n");
      out.write("\t\t\t\t\t\t\t\t<span>Need Help?</span>\n");
      out.write("\t\t\t\t\t\t\t\t<a href=\"#\" target=\"_blank\">Contact Support</a>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t</noscript>\n");
      out.write("\t\t\t\t\t\t<div id=\"SystemErrorContainer\"></div>\n");
      out.write("\t\t\t\t\t\t<div id=\"ErrorContainer\"></div>\n");
      out.write("\t\t\t\t\t\t<div class=\"form-row\">\n");
      out.write("\t\t\t\t\t\t\t<span class=\"label\">Email</span>\n");
      out.write("                                                        <input id=\"EmailField\" type=\"email\" name=\"Email\" class=\"field\" tabindex=\"1\" minlength=\"11\" maxlength=\"60\" autocomplete=\"off\" required=\"required\"/>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<div class=\"form-row\">\n");
      out.write("\t\t\t\t\t\t\t<span class=\"label\">Password</span> <a class=\"forgot-password-link\" href=\"http://localhost:8080/egertonfinder/forgotpassword/resetpassword.jsp\" target=\"_blank\">Forgot Your Password?</a>\n");
      out.write("                                                        <input id=\"PasswordField\" type=\"password\" name=\"Password\" class=\"field\" tabindex=\"2\" autocomplete=\"off\" minlength=\"6\" maxlength=\"60\" autocomplete=\"off\" required=\"required\"/>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t<div class=\"form-row action\">\n");
      out.write("\t\t\t\t\t\t\t<input id=\"LoginButton\" name=\"LoginButton\" type=\"submit\" value=\"Log In\" class=\"button-primary large-wide\" tabindex=\"4\" />\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<!--<input id=\"redirectPath\" name=\"redirectPath\" type=\"hidden\" value=\"#\" />-->\n");
      out.write("\t\t\t\t\t\t<div>\n");
      out.write("\t\t\t\t\t\t\t<input type=\"checkbox\" id=\"RememberMe\" class=\"\" tabindex=\"3\" />\n");
      out.write("\t\t\t\t\t\t\t<label for=\"RememberMe\">Remember me</label>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t</form>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t\t<div id=\"second-login-step\" class=\"box hidden\"></div>\n");
      out.write("\t\t\t\t<div class=\"login-footer\">\n");
      out.write("\t\t\t\t\t<span>Don't Have Account & Not an Administrator?</span>\n");
      out.write("\t\t\t\t\t<div class=\"try-free-button\">\n");
      out.write("\t\t\t\t\t\t<a href=\"http://localhost:8080/egertonfinder/clients/signup.jsp\" target=\"_blank\">Clients Can Sign Up Here.</a>\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t</div>\n");
      out.write("\t\t</div>\n");
      out.write("\n");
      out.write("\n");
      out.write("\t\t<div class=\"right-side\">\n");
      out.write("                    <h3 style=\"color: #ffffff\">Egerton University Lost and Found System</h3>\n");
      out.write("                    <iframe class=\"fadeOut\" id=\"promoFrame\" src=\"content/html/banner.html\"  scrolling=\"no\"></iframe>\n");
      out.write("\t\t</div>\n");
      out.write("\n");
      out.write("\t\t<script id=\"LoginConfigJsonModel\" type=\"text/json\">\n");
      out.write("\t\t{\"systemRequirementsUrl\":\"https://howto.caspio.com/system-requirements/\"}\n");
      out.write("\t\t</script>\n");
      out.write("\t</div>\n");
      out.write("\t<div class=\"cb_footer\">\n");
      out.write("\t\t\n");
      out.write("<link rel=\"stylesheet\" href=\"content/css/footer.min.css\" />\n");
      out.write("\n");
      out.write("<div class=\"footer\" style=\"\">\n");
      out.write("\t<span>&copy; 2019  Efinder, Inc.</span>\n");
      out.write("\t<span class=\"additional-links\">\n");
      out.write("\t\t<a href=\"#\" target=\"_blank\">Privacy</a>\n");
      out.write("\t\t<span>|</span>\n");
      out.write("\t\t<a href=\"#\" target=\"_blank\">Terms</a>\n");
      out.write("\t</span>\n");
      out.write("</div>\n");
      out.write("\t</div>\n");
      out.write("</div>\n");
      out.write("</body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
