package org.apache.jsp.clients;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.time.*;
import java.text.*;
import java.time.format.*;
import admins.ConnectionManager;

public final class mpostfound_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write('\n');
      out.write('\n');

    String cfnames = (String)session.getAttribute("cfname");
    String clnames  = (String)session.getAttribute("clname");
    String cmails = (String)session.getAttribute("cmail");
    String cphones = (String)session.getAttribute("cphone");
    String cnationals  = (String)session.getAttribute("cnational");
    String croles = (String)session.getAttribute("crole");
    
        String founder = cfnames + " "+ clnames;
        
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
	Date date = new Date();
        String datereported = dateFormat.format(date);
        
        DateFormat dateFormat2 = new SimpleDateFormat("HH:mm:ss");
	Date date2 = new Date();
        String timereported = dateFormat2.format(date2);
    
      out.write('\n');
      out.write('\n');
            
              
//    FileInputStream fis=null;
    PreparedStatement pstmt = null;
    ResultSet rs=null;
    Connection conn=null;
                        String itname = request.getParameter("itname");
                        String place = request.getParameter("placefound");
                        String datefound = request.getParameter("whenfound");
                        String describe = request.getParameter("description");
//                      String phon = request.getParameter("phone");
                       // String myloc=request.getParameter("userimg");
                        
            //Tracks whether the form can insert the secutity admin to the database.
             System.out.println("The item name is  " + itname); 
             System.out.println("The place where the item was found is  " + place);
             System.out.println("The date when the item was founf is " + datefound); 
             System.out.println("The item description is " + describe);
             //System.out.println("Your user phone is " + phon); 
             
             
             
             
            try
                {
            ConnectionManager manager= new ConnectionManager();  
             conn = manager.getConnect();    

                       pstmt = conn.prepareStatement("INSERT INTO founditems (itemname,description, placefound, datefound, datereported, timereported, foundername, founderphone, founderemail, founderId, founderrole) " + "VALUES(?,?,?,?,?,?,?,?,?,?,?)");
                        
                      //Item Details
                        pstmt.setString(1, itname);
                        pstmt.setString(2, describe);
                        pstmt.setString(3, place);
                        pstmt.setString(4, datefound);
                        
                        //Founder Details
                        pstmt.setString(5, datereported);
                        pstmt.setString(6, timereported);
                        pstmt.setString(7, founder);
                        pstmt.setString(8, cphones);
                        pstmt.setString(9, cmails);
                        pstmt.setString(10, cnationals);
                        pstmt.setString(11, croles);
//                        fis=new FileInputStream(image);
//                        pstmt.setBinaryStream(9, (InputStream) fis, (int) (image.length()));
                        int count = pstmt.executeUpdate();
                        if(count>0)
                            {
                     
                      System.out.println("Data was successfully inserted!");
                      
                      String redirectURL = "http://localhost:8080/egertonfinder/clients/postfounditemsuccess.jsp?Found property added successfully";
                      response.sendRedirect(redirectURL);
                      } 
                        else if(count<=0){
                            System.out.println("Data insertion failed!");
                      
                      String redirectURL1 = "http://localhost:8080/egertonfinder/clients/postfounditemfail.jsp? Posting of found item failed!";
                      response.sendRedirect(redirectURL1);

                        }
                        else{
                            
                        }
                }
                        catch(Exception e)
                        {
                        System.out.print(e);
                        e.printStackTrace();
                        }
                                finally{
                            try{
                            if(rs!=null){
                            rs.close();
                            rs= null;
                            }
                            if(pstmt !=null)
                            {
                            pstmt.close();
                            pstmt=null;
                            }
                            if(conn!=null)
                            {
                            conn.close();
                            conn=null;
                            }
                            }
                            catch(Exception e)
                            {
                            e.printStackTrace();
                            }
                            }
    
        
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
