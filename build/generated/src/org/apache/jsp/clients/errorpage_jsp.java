package org.apache.jsp.clients;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class errorpage_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    Throwable exception = org.apache.jasper.runtime.JspRuntimeLibrary.getThrowable(request);
    if (exception != null) {
      response.setStatus((Integer)request.getAttribute("javax.servlet.error.status_code"));
    }
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<!--\n");
      out.write("To change this license header, choose License Headers in Project Properties.\n");
      out.write("To change this template file, choose Tools | Templates\n");
      out.write("and open the template in the editor.\n");
      out.write("-->\n");
      out.write("<html class=\"gr__colorlib_com\" lang=\"en\"><head>\n");
      out.write("\t<meta charset=\"utf-8\">\n");
      out.write("\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n");
      out.write("\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n");
      out.write("\t<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->\n");
      out.write("\n");
      out.write("\t<title>Error 404! The page requested is not available</title>\n");
      out.write("\n");
      out.write("\t<!-- Google font -->\n");
      out.write("\t<link href=\"https://fonts.googleapis.com/css?family=Montserrat:400,700,900\" rel=\"stylesheet\">\n");
      out.write("\n");
      out.write("\t<!-- Custom stlylesheet -->\n");
      out.write("\t<link type=\"text/css\" rel=\"stylesheet\" href=\"../content/css/style.css\">\n");
      out.write("\n");
      out.write("\t<style>\n");
      out.write("            marquee {\n");
      out.write("        width: 100%;\n");
      out.write("        padding: 2px 0;\n");
      out.write("        background-color: #006699;\n");
      out.write("      }\n");
      out.write("            \n");
      out.write("            </style>\n");
      out.write("\n");
      out.write("</head>\n");
      out.write("\n");
      out.write("<body data-gr-c-s-loaded=\"true\">\n");
      out.write("    <div id=\"animatingnav\">\n");
      out.write("        <marquee direction=\"right\"> <h2 style=\"color: #ffffff\">Egerton University Lost And Found Systems.</h2></marquee>\n");
      out.write("    </div>\n");
      out.write("\t<div id=\"notfound\">\n");
      out.write("\t\t<div class=\"notfound\">\n");
      out.write("\t\t\t<div class=\"\">\n");
      out.write("                            <img src=\"../content/images/oops.jpg\" alt=\"O0ps\" height=\"200\" width=\"200\"> \n");
      out.write("\t\t\t</div>\n");
      out.write("\t\t\t<h2>404 - Page not found !!</h2>\n");
      out.write("\t\t\t<p>The page you are looking for might have been removed, had its name changed or is temporarily unavailable. We are sorry for any inconveniences.</p>\n");
      out.write("\t\t\t<a href=\"home\">Go To Homepage</a>\n");
      out.write("\t\t</div>\n");
      out.write("\t</div>\n");
      out.write("\n");
      out.write("<!-- Global site tag (gtag.js) - Google Analytics -->\n");
      out.write("<script type=\"text/javascript\" async=\"\" src=\"https://www.google-analytics.com/analytics.js\">\n");
      out.write("<script type=\"text/javascript\" async=\"\" src=\"content/js/analytics.js\">\n");
      out.write("</script><script async=\"\" src=\"https://www.googletagmanager.com/gtag/js?id=UA-23581568-13\" type=\"text/javascript\"></script>\n");
      out.write("<script type=\"text/javascript\">\n");
      out.write("  window.dataLayer = window.dataLayer || [];\n");
      out.write("  function gtag(){dataLayer.push(arguments);}\n");
      out.write("  gtag('js', new Date());\n");
      out.write("\n");
      out.write("  gtag('config', 'UA-23581568-13');\n");
      out.write("</script>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("</body><!-- This templates was made by Colorlib (https://colorlib.com) --></html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
