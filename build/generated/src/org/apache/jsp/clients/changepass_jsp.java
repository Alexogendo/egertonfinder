package org.apache.jsp.clients;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.*;
import java.sql.Connection;
import admins.ConnectionManager;

public final class changepass_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");

    String cids = (String)session.getAttribute("cid");
    
    
                            String oldp = request.getParameter("oldpass");
                            String newp = request.getParameter("newpass");
                            String conp = request.getParameter("conpass");
                
                    int newid = Integer.parseInt(cids);
                        if(cids != null)
                        {
                            
                        Connection conn= null;
                        PreparedStatement ps = null;
                        Statement st = null;
                        ResultSet rs = null;
                       
                        try{
                            
                            ConnectionManager manager= new ConnectionManager();  
                             conn = manager.getConnect();
                             st=conn.createStatement();
                                    String strQuery = "SELECT password FROM users where userId='"+newid+"'";
                                     rs = st.executeQuery(strQuery);
                                     rs.next();
                                     String pass = rs.getString(1);
                                     
                                     if((oldp.equals(pass)) && (newp.equals(conp))){
                                         String sql="Update users set password=? where userId='"+ newid +"'";
                                                ps = conn.prepareStatement(sql);
                                                ps.setString(1,newp);
                              
                                    int i = ps.executeUpdate();
                                    if(i > 0)
                                    {
                                        
                                         String redirectURL = "http://localhost:8080/egertonfinder/clients/changepasswordsuccess.jsp?Your password was changed successfully";
                                        
                                        response.sendRedirect(redirectURL);
                                     }
                                    else
                                    {
                                   // out.print("There is a problem in updating Record.");
                                    String redirectURL2 = "http://localhost:8080/egertonfinder/clients/changepasswordfail.jsp?Password change failed. Check your details and try again!";
                                    response.sendRedirect(redirectURL2);
                                    }
                                    }
                                     else
                                    {
                                   // out.print("There is a problem in updating Record.");
                                    String redirectURL1 = "http://localhost:8080/egertonfinder/clients/changepasswordfail.jsp?Password change failed. Check your details and try again!";
                                    response.sendRedirect(redirectURL1);
                                    }
                                     }
                                    catch(SQLException sql)
                                            {
                                    request.setAttribute("error", sql);
                                    out.println(sql);
                                            }
                                        }
                                    else
                                    {
                                   // out.print("There is a problem in updating Record.");
                                    String redirectURL3 = "http://localhost:8080/egertonfinder/clients/changepasswordfail.jsp?Password change failed. Check your details and try again!";
                                    response.sendRedirect(redirectURL3);
                                    }

    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
