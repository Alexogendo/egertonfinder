package org.apache.jsp.clients;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.time.*;
import java.text.*;
import java.time.format.*;
import admins.ConnectionManager;

public final class mfounditupdated_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");

    String cfnames = (String)session.getAttribute("cfname");
    String clnames  = (String)session.getAttribute("clname");
    String cmails = (String)session.getAttribute("cmail");
    String cphones = (String)session.getAttribute("cphone");
    String cnationals  = (String)session.getAttribute("cnational");
    String croles = (String)session.getAttribute("crole");
    
        String founder = cfnames + " "+ clnames;
        
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
	Date date = new Date();
        String datereported = dateFormat.format(date);
        
        DateFormat dateFormat2 = new SimpleDateFormat("HH:mm:ss");
	Date date2 = new Date();
        String timereported = dateFormat2.format(date2);
    
      out.write('\n');
      out.write('\n');

    
    
                        String itemid = request.getParameter("itemid");
                        String a=request.getParameter("itname");
                        String b=request.getParameter("description");
                        String c=request.getParameter("placefound");
                        String d=request.getParameter("whenfound");
                        String d2=request.getParameter("stat");
                        String e=request.getParameter("lname");
                        String f=request.getParameter("lphone");
                        String g=request.getParameter("lmail");
                        String h=request.getParameter("lid");
                        String w=request.getParameter("role");
                        
      out.write("\n");
      out.write("                        \n");
      out.write("                        \n");
      out.write("                        ");

                        
                        if(itemid != null)
                        {
                            
                        Connection conn=null;
                        PreparedStatement ps = null;
                        //int itId = Integer.parseInt(id);
                        try
                        {
                            ConnectionManager manager= new ConnectionManager();  
                             conn = manager.getConnect();
                             
                             String sql="Update lostitems set itemid=?,itemname=?, description=?,placelost=?,datelost=? ,status=?, losername=?, loserphone=?, loseremail=?, loserid=? , loserrole=? where itemid="+itemid;
                                    ps = conn.prepareStatement(sql);
                                    ps.setString(1,itemid);
                                    ps.setString(2, a);
                                    ps.setString(3, b);
                                    ps.setString(4, c);
                                    ps.setString(5, d);
                                    ps.setString(6,d2);
                                    ps.setString(7, e);
                                    ps.setString(8, f);
                                    ps.setString(9, g);
                                    ps.setString(10, h);
                                    ps.setString(11, w);
                                    int i = ps.executeUpdate();
                                    if(i > 0)
                                    {
                                  // out.print("Record Updated Successfully");
                                    
                                   
                            ConnectionManager m= new ConnectionManager();  
                            conn = m.getConnect();
                             
                             ps = conn.prepareStatement("INSERT INTO founditems (itemname, description, placefound, datefound, losername, loserphone, loseremail, loserid , loserrole ,datereported, timereported, foundername, founderphone, founderemail, founderId, founderrole) " + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                     // int i=st.executeUpdate("INSERT INTO admins (firstname,lastname, password, email, phone, idNo, role, department) VALUES ('"+fname+"','"+lname+"','"+pass+"','"+mail+"','"+phon+"','"+nId+"','"+rol+"','"+depart+"')");
                         
                                    ps.setString(1, a);
                                    ps.setString(2, b);
                                    ps.setString(3, c);
                                    ps.setString(4, d);
                                    ps.setString(5, e);
                                    ps.setString(6, f);
                                    ps.setString(7, g);
                                    ps.setString(8, h);
                                    ps.setString(9, w);
                                    
                                    //Founder Details
                                    ps.setString(10, datereported);
                                    ps.setString(11, timereported);
                                    ps.setString(12, founder);
                                    ps.setString(13, cphones);
                                    ps.setString(14, cmails);
                                    ps.setString(15, cnationals);
                                    ps.setString(16, croles);
                                    
//                        fis=new FileInputStream(image);
//                        pstmt.setBinaryStream(9, (InputStream) fis, (int) (image.length()));
                        int count = ps.executeUpdate();
                        if(count>0)
                            {
                     
                      //System.out.println("Data was successfully inserted!");
                      
                     String redirectURL = "http://localhost:8080/egertonfinder/clients/mfounditsuccess.jsp?Item has been posted";
                     response.sendRedirect(redirectURL);
                      } 
                        else{
                           // System.out.println("Data insertion failed!");
                      
                      String redirectURL1 = "http://localhost:8080/egertonfinder/clients/mfounditfail.jsp?Posting of the item failed. Try again!";
                      response.sendRedirect(redirectURL1);
                        }
                                    
                                    }
                                    else
                                    {
                                   // out.print("There is a problem in updating Record.");
                                    String redirectURL2 = "http://localhost:8080/egertonfinder/clients/mfounditfail.jsp?Posting of the item failed. Try again!";
                                    response.sendRedirect(redirectURL2);
                                    }
                                    }
                                    catch(SQLException sql)
                                    {
                                    request.setAttribute("error", sql);
                                    out.println(sql);
                                    }
                                    }
                                    
      out.write(' ');
      out.write('\n');
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
