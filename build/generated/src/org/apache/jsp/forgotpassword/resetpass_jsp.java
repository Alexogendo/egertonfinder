package org.apache.jsp.forgotpassword;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.mail.internet.*;
import javax.activation.*;
import javax.servlet.http.*;
import javax.servlet.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.time.*;
import java.text.*;
import java.time.format.*;
import admins.ConnectionManager;

public final class resetpass_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<meta http-equiv=\"Refresh\" content=\"8;resetpassword.jsp\">\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");

    //Creating a result for getting status that messsage is delivered or not!
    String result;
    // Get recipient's email-ID, message & subject-line from index.html page
    final String to = request.getParameter("email");
    
    //Some code to test whether an email exists here first before sending a password
    ResultSet rs=null;
    Connection conn=null;
     try
        {
            ConnectionManager manager= new ConnectionManager();  
             conn = manager.getConnect();    
            
            Statement st1=conn.createStatement();
             String strQuery = "SELECT COUNT(*) FROM users where email='"+to+"'";
                        rs = st1.executeQuery(strQuery);
                        rs.next();
                        String Countrow = rs.getString(1);
                        System.out.println(Countrow);
                        int count = Integer.parseInt(Countrow);
                        
                        if(count > 0)
                        {
    
    final String subject = "PASSWORD RESET";
    final String messg = "<h4>Hello "+to+", we received a request to reset your password. If this was you, please click the "
            + "link below to reset your password.<br>"
            + "</h4><a href=" +"http://localhost:8080/egertonfinder/forgotpassword/resettingpass.jsp?email="+to+"> Click here.</a>"
            + "<br>"
            + "<h3>Kind Regards,</h3>\n"
            + "<strong><i>Egerton Lost & Found Systems.</i></strong>";
    
    //messg.setContent("text/html");
    
 
        // Sender's email ID and password needs to be mentioned
    final String from = "egertonfinders@gmail.com";
    final String pass = "Underscore10#";
 
 
    // Defining the gmail host
    String host = "smtp.gmail.com";
 
    // Creating Properties object
    Properties props = new Properties();
 
    // Defining properties
    props.put("mail.smtp.host", host);
    props.put("mail.transport.protocol", "smtp");
    props.put("mail.smtp.auth", "true");
    props.put("mail.smtp.starttls.enable", "true");
    props.put("mail.user", from);
    props.put("mail.password", pass);
    props.put("mail.port", "465");
 
    // Authorized the Session object.
    Session mailSession = Session.getInstance(props, new javax.mail.Authenticator() {
        @Override
        protected PasswordAuthentication getPasswordAuthentication() {
            return new PasswordAuthentication(from, pass);
        }
    });
 
    try {
        // Create a default MimeMessage object.
        MimeMessage message = new MimeMessage(mailSession);
        // Set From: header field of the header.
        message.setFrom(new InternetAddress(from));
        // Set To: header field of the header.
        message.addRecipient(Message.RecipientType.TO,
                new InternetAddress(to));
        // Set Subject: header field
        message.setSubject(subject);
        // Now set the actual message
        message.setContent(messg,"text/html");
        // Send message
        Transport.send(message);
        result = "Reset password email sent successfully to your account!";
    } 
    catch (MessagingException mex) {
        mex.printStackTrace();
        result = "Error: unable to send the reset password mail....";
    }

      out.write("\n");
      out.write("<title>Egerton Lost and Found Systems</title>\n");
      out.write("<center><h2><font color=\"blue\">Thank you for being our member.</font></h2>\n");
      out.write("<b><font color=\"green\">");
 out.println(result);
      out.write("</b>\n");
      out.write("</center>\n");

    
                        }//End of if statement
                        else{
                        System.out.println("The Email entered does not exist!");
                        
                        String redirectURL2 = "http://localhost:8080/egertonfinder/forgotpassword/emailnotfound.jsp?The email entered does not exist";
                        response.sendRedirect(redirectURL2);
                        }

}
                        catch(Exception e)
                        {
                        System.out.print(e);
                        e.printStackTrace();
                        }
    
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
