package org.apache.jsp.admins;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.*;
import java.util.*;
import admins.ConnectionManager;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.time.*;
import java.text.*;
import java.time.format.*;
import admins.ConnectionManager;

public final class mregisterclients_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write('\n');
      out.write('\n');
            
       DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
	Date date = new Date();
        String createdate = dateFormat.format(date);
        
        DateFormat dateFormat2 = new SimpleDateFormat("HH:mm:ss");
	Date date2 = new Date();
        String createtime = dateFormat2.format(date2);
        
        
//    FileInputStream fis=null;
    PreparedStatement pstmt = null;
    ResultSet rs=null;
    Connection conn=null;
                        String pass = request.getParameter("tempass");
                        String mail = request.getParameter("email");
                        String fname = request.getParameter("firstname");
                        String lname = request.getParameter("lastname");
                        String phon = request.getParameter("phone");
                        String nId = request.getParameter("id");
                        String rol = request.getParameter("role");
                        String depart = request.getParameter("department");
                       // String myloc=request.getParameter("userimg");
            
    try
        {
            ConnectionManager manager= new ConnectionManager();  
             conn = manager.getConnect();    
            
            Statement st1=conn.createStatement();
             String strQuery = "SELECT COUNT(*) FROM users where email='"+mail+"'";
                        rs = st1.executeQuery(strQuery);
                        rs.next();
                        String Countrow = rs.getString(1);
                        System.out.println(Countrow);
                        if(Countrow.equals("0"))
                        {
            //Create sql statement and insert the security administrator
                           // File image= new File(myloc);
                     // Statement st=conn.createStatement();
                      pstmt = conn.prepareStatement("INSERT INTO users (firstname,lastname, password, email, phone, idNo, role, department, datecreated, timecreated) " + "VALUES(?,?,?,?,?,?,?,?,?,?)");
                     // int i=st.executeUpdate("INSERT INTO admins (firstname,lastname, password, email, phone, idNo, role, department) VALUES ('"+fname+"','"+lname+"','"+pass+"','"+mail+"','"+phon+"','"+nId+"','"+rol+"','"+depart+"')");
                        pstmt.setString(1, fname);
                        pstmt.setString(2, lname);
                        pstmt.setString(3, pass);
                        pstmt.setString(4, mail);
                        pstmt.setString(5, phon);
                        pstmt.setString(6, nId);
                        pstmt.setString(7, rol);
                        pstmt.setString(8, depart);
                        
                        pstmt.setString(9, createdate);
                        pstmt.setString(10, createtime);
//                        fis=new FileInputStream(image);
//                        pstmt.setBinaryStream(9, (InputStream) fis, (int) (image.length()));
                        int count = pstmt.executeUpdate();
                        if(count>0)
                            {
                     
                      System.out.println("Data was successfully inserted!");
                      
                      String redirectURL = "http://localhost:8080/egertonfinder/admins/registerclientsssuccess.jsp?Client has been added successfully";
                      response.sendRedirect(redirectURL);
                      } 
                        else{
                            System.out.println("Data insertion failed!");
                      
                      String redirectURL1 = "http://localhost:8080/egertonfinder/errorpage.jsp? We have noticed ubnormal behavior!";
                      response.sendRedirect(redirectURL1);
                        }
                        }//End of if statement
                        else{
                        System.out.println("The national id number or Email already exists !");
                        
                        String redirectURL2 = "http://localhost:8080/egertonfinder/admins/registerclientsfail.jsp?Client registration failed";
                        response.sendRedirect(redirectURL2);
                        }

                        }
                        catch(Exception e)
                        {
                        System.out.print(e);
                        e.printStackTrace();
                        }
                                finally{
                            try{
                            if(rs!=null){
                            rs.close();
                            rs= null;
                            }
                            if(pstmt !=null)
                            {
                            pstmt.close();
                            pstmt=null;
                            }
                            if(conn!=null)
                            {
                            conn.close();
                            conn=null;
                            }
                            }
                            catch(Exception e)
                            {
                            e.printStackTrace();
                            }
                            }
    
        
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
