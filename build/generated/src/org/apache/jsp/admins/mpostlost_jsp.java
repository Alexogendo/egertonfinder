package org.apache.jsp.admins;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.*;
import java.util.*;
import admins.ConnectionManager;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.time.*;
import java.text.*;
import java.time.format.*;
import admins.ConnectionManager;

public final class mpostlost_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write('\n');
      out.write('\n');

    //String ids = (String)session.getAttribute("id");
    String sfnames = (String)session.getAttribute("sfname");
    String slnames  = (String)session.getAttribute("slname");
    String smails = (String)session.getAttribute("smail");
    String sphones = (String)session.getAttribute("sphone");
    String snationals  = (String)session.getAttribute("snational");
    String sroles = (String)session.getAttribute("srole");
    

        String loser = sfnames + " "+ slnames;
        
         DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
	Date date = new Date();
        String datereported = dateFormat.format(date);
        
        DateFormat dateFormat2 = new SimpleDateFormat("HH:mm:ss");
	Date date2 = new Date();
        String timereported = dateFormat2.format(date2);
    
      out.write('\n');
      out.write('\n');
            
              
//    FileInputStream fis=null;
    PreparedStatement pstmt = null;
    ResultSet rs=null;
    Connection conn=null;
                        String itname = request.getParameter("itname");
                        String place = request.getParameter("placelost");
                        String datelost = request.getParameter("whenlost");
                        String describe = request.getParameter("description");
//                      String phon = request.getParameter("phone");
                       // String myloc=request.getParameter("userimg");
                        
            //Tracks whether the form can insert the secutity admin to the database.
             System.out.println("The item name is  " + itname); 
             System.out.println("The place where the item was lost is  " + place);
             System.out.println("The date when the item was lost is " + datelost); 
             System.out.println("The item description is " + describe);
             //System.out.println("Your user phone is " + phon); 
              
             
            try
                {
            ConnectionManager manager= new ConnectionManager();  
             conn = manager.getConnect();    

                     pstmt = conn.prepareStatement("INSERT INTO lostitems (itemname,description, placelost, datelost, datereported, timereported, losername, loserphone, loseremail, loserid, loserrole) " + "VALUES(?,?,?,?,?,?,?,?,?,?,?)");
                       
                      //Lost Item details
                        pstmt.setString(1, itname);
                        pstmt.setString(2, describe);
                        pstmt.setString(3, place);
                        pstmt.setString(4, datelost);
                        
                        //Loser details
                        pstmt.setString(5, datereported);
                        pstmt.setString(6, timereported);
                        pstmt.setString(7, loser);
                        pstmt.setString(8, sphones);
                        pstmt.setString(9, smails);
                        pstmt.setString(10, snationals);
                        pstmt.setString(11, sroles);
                        
//                        fis=new FileInputStream(image);
//                        pstmt.setBinaryStream(9, (InputStream) fis, (int) (image.length()));
                        int count = pstmt.executeUpdate();
                        if(count>0)
                            {
                     
                      System.out.println("Data was successfully inserted!");
                      
                      String redirectURL = "http://localhost:8080/egertonfinder/admins/postlostitemsuccess.jsp?Found property added successfully";
                      response.sendRedirect(redirectURL);
                      } 
                        else if(count<=0){
                            System.out.println("Data insertion failed!");
                      
                      String redirectURL1 = "http://localhost:8080/egertonfinder/admins/postlostitemfail.jsp? Posting of found item failed!";
                      response.sendRedirect(redirectURL1);

                        }
                        else{
                            
                        }
                }
                        catch(Exception e)
                        {
                        System.out.print(e);
                        e.printStackTrace();
                        }
                                finally{
                            try{
                            if(rs!=null){
                            rs.close();
                            rs= null;
                            }
                            if(pstmt !=null)
                            {
                            pstmt.close();
                            pstmt=null;
                            }
                            if(conn!=null)
                            {
                            conn.close();
                            conn=null;
                            }
                            }
                            catch(Exception e)
                            {
                            e.printStackTrace();
                            }
                            }
    
        
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
