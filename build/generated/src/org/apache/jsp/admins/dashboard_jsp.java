package org.apache.jsp.admins;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import admins.AdminBean;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Connection;
import admins.ConnectionManager;

public final class dashboard_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("  \n");
      out.write("\n");
      out.write("  \n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
 
                
                //session.setMaxInactiveInterval(1800);
               AdminBean currentSec = ((AdminBean)(session.getAttribute("currentSessionSec")));
      out.write("\n");
      out.write("               \n");
      out.write("               ");

                   String sid = currentSec.getId(); 
                   String sfname = currentSec.getFirstname();
                   String slname = currentSec.getLastname();
                   String smail = currentSec.getEmail();
                   String sphone = currentSec.getPhone();
                   String snational = currentSec.getNationalId();
                   String srole = currentSec.getRole();
                   
                   //int mId = Integer.parseInt(id);
                   
                   session.setAttribute("sid", sid);
                   session.setAttribute("sfname", sfname);
                   session.setAttribute("slname", slname);
                   session.setAttribute("smail", smail);
                   session.setAttribute("sphone", sphone);
                   session.setAttribute("snational", snational);
                   session.setAttribute("srole", srole);   
                   
if (srole.equals("Securityadmin")){
                   
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html lang=\"en\">\n");
      out.write("\n");
      out.write("<head>\n");
      out.write("  <meta charset=\"utf-8\" />\n");
      out.write("  <link rel=\"apple-touch-icon\" sizes=\"76x76\" href=\"../content/images/img_467397.png\">\n");
      out.write("    <link rel=\"icon\" type=\"image/png\" sizes=\"96x96\" href=\"../content/images/img_467397.png\">\n");
      out.write("    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\" />\n");
      out.write("  <title>\n");
      out.write("    Security administrator Logged Successfully \n");
      out.write("  </title>\n");
      out.write("  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />\n");
      out.write("  <!--     Fonts and icons     -->\n");
      out.write("  <link href=\"https://fonts.googleapis.com/css?family=Montserrat:400,700,200\" rel=\"stylesheet\" />\n");
      out.write("  <link href=\"https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css\" rel=\"stylesheet\">\n");
      out.write("  <!-- CSS Files -->\n");
      out.write("  <link href=\"../assets/css/bootstrap.min.css\" rel=\"stylesheet\" />\n");
      out.write("  <link href=\"../assets/css/paper-dashboard.css?v=2.0.0\" rel=\"stylesheet\" />\n");
      out.write("  <!-- CSS Just for demo purpose, don't include it in your project -->\n");
      out.write("  <link href=\"../assets/demo/demo.css\" rel=\"stylesheet\" />\n");
      out.write("  \n");
      out.write("  <style>\n");
      out.write("     #onhoverLogout{\n");
      out.write("                visibility: hidden;\n");
      out.write("            }\n");
      out.write("            #onlogoutIcon:hover ~ #onhoverLogout{\n");
      out.write("                visibility: visible;\n");
      out.write("            }\n");
      out.write("    </style>\n");
      out.write("</head>\n");
      out.write("\n");
      out.write("<body class=\"\" onload=\"demo.showNotification('top','center')\">\n");
      out.write("  \n");
      out.write("    \n");
      out.write("  <div class=\"wrapper \">\n");
      out.write("    <div class=\"sidebar\" data-color=\"green\" data-active-color=\"danger\">\n");
      out.write("      <!--\n");
      out.write("        Tip 1: You can change the color of the sidebar using: data-color=\"blue | green | orange | red | yellow\"\n");
      out.write("    -->\n");
      out.write("      <div class=\"logo\">\n");
      out.write("        <a href=\"http://www.creative-tim.com\" class=\"simple-text logo-mini\">\n");
      out.write("          <div class=\"logo-image-small\">\n");
      out.write("            <img src=\"../content/images/img_467397.png\">\n");
      out.write("          </div>\n");
      out.write("        </a>\n");
      out.write("        <a href=\"http://www.creative-tim.com\" class=\"simple-text logo-normal\">\n");
      out.write("         Efinder\n");
      out.write("          <!-- <div class=\"logo-image-big\">\n");
      out.write("            <img src=\"../assets/img/logo-big.png\">\n");
      out.write("          </div> -->\n");
      out.write("        </a>\n");
      out.write("      </div>\n");
      out.write("      <div class=\"sidebar-wrapper\">\n");
      out.write("         <ul class=\"nav\" style=\"\">\n");
      out.write("          <li class=\"hove\">\n");
      out.write("            <a href=\"./dashboard.jsp\">\n");
      out.write("              <i class=\"nc-icon nc-bank\"></i>\n");
      out.write("              <p>Dashboard</p>\n");
      out.write("            </a>\n");
      out.write("          </li>\n");
      out.write("          \n");
      out.write("          <li class=\"hove\">\n");
      out.write("            <a href=\"./postfounditem.jsp\">\n");
      out.write("              <i class=\"nc-icon nc-sound-wave\"></i>\n");
      out.write("              <p>Post Found Item</p>\n");
      out.write("            </a>\n");
      out.write("          </li>\n");
      out.write("          <li class=\"hove\">\n");
      out.write("            <a href=\"./postlostitem.jsp\">\n");
      out.write("              <i class=\"nc-icon nc-sun-fog-29\"></i>\n");
      out.write("              <p>Post Lost Item</p>\n");
      out.write("            </a>\n");
      out.write("          </li>\n");
      out.write("          <li class=\"hove\">\n");
      out.write("            <a href=\"./assignitems.jsp\">\n");
      out.write("              <i class=\"nc-icon nc-bulb-63\"></i>\n");
      out.write("              <p>Assign Items to Owners</p>\n");
      out.write("            </a>\n");
      out.write("          </li>\n");
      out.write("          <li class=\"hove\">\n");
      out.write("            <a href=\"./entries.jsp\">\n");
      out.write("              <i class=\"nc-icon nc-tile-56\"></i>\n");
      out.write("              <p>System Entries</p>\n");
      out.write("            </a>\n");
      out.write("          </li>\n");
      out.write("  \n");
      out.write("          <li class=\"hove\">\n");
      out.write("            <a href=\"./registerclients&pcs.jsp\">\n");
      out.write("              <i class=\"nc-icon nc-single-copy-04\"></i>\n");
      out.write("              <p>Register Clients & Pc's</p>\n");
      out.write("            </a>\n");
      out.write("          </li>\n");
      out.write("          <li class=\"hove\">\n");
      out.write("            <a href=\"./user.jsp\">\n");
      out.write("              <i class=\"nc-icon nc-single-02\"></i>\n");
      out.write("              <p>User Profile</p>\n");
      out.write("            </a>\n");
      out.write("          </li>\n");
      out.write("          \n");
      out.write("          <li class=\"hove\">\n");
      out.write("            <a href=\"http://localhost:8080/egertonfinder/Help/index.html\">\n");
      out.write("              <i class=\"nc-icon nc-spaceship\"></i>\n");
      out.write("              <p>Gain Experience</p>\n");
      out.write("            </a>\n");
      out.write("          </li>\n");
      out.write("        </ul>\n");
      out.write("      </div>\n");
      out.write("    </div>\n");
      out.write("    <div class=\"main-panel\">\n");
      out.write("      <!-- Navbar -->\n");
      out.write("      <nav class=\"navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent\">\n");
      out.write("        <div class=\"container-fluid\">\n");
      out.write("          <div class=\"navbar-wrapper\">\n");
      out.write("            <div class=\"navbar-toggle\">\n");
      out.write("              <button type=\"button\" class=\"navbar-toggler\">\n");
      out.write("                <span class=\"navbar-toggler-bar bar1\"></span>\n");
      out.write("                <span class=\"navbar-toggler-bar bar2\"></span>\n");
      out.write("                <span class=\"navbar-toggler-bar bar3\"></span>\n");
      out.write("              </button>\n");
      out.write("            </div>\n");
      out.write("            <a class=\"navbar-brand\" href=\"#pablo\">Egerton Lost and Found Systems</a> \n");
      out.write("            <p style=\"color: forestgreen;font-size: 15px; font-style:italic\">  \n");
      out.write("                   \n");
      out.write("               Welcome: ");
      out.print( currentSec.getFirstname() + " " + currentSec.getLastname() );
      out.write(" </p>\n");
      out.write("\n");
      out.write("          </div>\n");
      out.write("          <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navigation\" aria-controls=\"navigation-index\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n");
      out.write("            <span class=\"navbar-toggler-bar navbar-kebab\"></span>\n");
      out.write("            <span class=\"navbar-toggler-bar navbar-kebab\"></span>\n");
      out.write("            <span class=\"navbar-toggler-bar navbar-kebab\"></span>\n");
      out.write("          </button>\n");
      out.write("          <div class=\"collapse navbar-collapse justify-content-end\" id=\"navigation\">\n");
      out.write("              \n");
      out.write("            <form>\n");
      out.write("              <div class=\"input-group no-border\">\n");
      out.write("                <input type=\"text\" value=\"\" class=\"form-control\" placeholder=\"Search...\">\n");
      out.write("                <div class=\"input-group-append\">\n");
      out.write("                  <div class=\"input-group-text\">\n");
      out.write("                    <i class=\"nc-icon nc-zoom-split\"></i>\n");
      out.write("                  </div>\n");
      out.write("                </div>\n");
      out.write("              </div>\n");
      out.write("            </form>\n");
      out.write("            <ul class=\"navbar-nav\">\n");
      out.write("<!--              <li class=\"nav-item\">\n");
      out.write("                <a class=\"nav-link btn-magnify\" href=\"#pablo\">\n");
      out.write("                  <i class=\"nc-icon nc-layout-11\"></i>\n");
      out.write("                  <p>\n");
      out.write("                    <span class=\"d-lg-none d-md-block\">Stats</span>\n");
      out.write("                  </p>\n");
      out.write("                </a>\n");
      out.write("              </li>-->\n");
      out.write("              <li class=\"nav-item btn-rotate dropdown\">\n");
      out.write("                <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n");
      out.write("                  <i class=\"nc-icon nc-bell-55\"></i>\n");
      out.write("                  <p>\n");
      out.write("                    <span class=\"d-lg-none d-md-block\">Some Actions</span>\n");
      out.write("                  </p>\n");
      out.write("                </a>\n");
      out.write("                <div class=\"dropdown-menu dropdown-menu-right\" aria-labelledby=\"navbarDropdownMenuLink\">\n");
      out.write("                  <a class=\"dropdown-item\" href=\"./notifications.jsp\">Notifications</a>\n");
      out.write("                  <a class=\"dropdown-item\" href=\"./user.jsp\">Your Profile</a>\n");
      out.write("                  <a class=\"dropdown-item\" href=\"./changepassword.jsp\">Change Password</a>\n");
      out.write("                </div>\n");
      out.write("              </li>\n");
      out.write("              <li class=\"nav-item\">\n");
      out.write("                <a class=\"nav-link btn-rotate\" href=\"../logout\">\n");
      out.write("                    <div id=\"onlogoutIcon\">\n");
      out.write("                  <i class=\"nc-icon nc-button-power\"></i>\n");
      out.write("                    </div>\n");
      out.write("                  <div id=\"onhoverLogout\">Logout</div>\n");
      out.write("                  \n");
      out.write("                </a>\n");
      out.write("              </li>\n");
      out.write("            </ul>\n");
      out.write("          </div>\n");
      out.write("        </div>\n");
      out.write("      </nav>\n");
      out.write("      <!-- End Navbar -->\n");
      out.write("      <!-- <div class=\"panel-header panel-header-lg\">\n");
      out.write("  \n");
      out.write("  <canvas id=\"bigDashboardChart\"></canvas>\n");
      out.write("  \n");
      out.write("  \n");
      out.write("</div> -->\n");
      out.write("      <div class=\"content\">\n");
      out.write("        <div class=\"row\">\n");
      out.write("            \n");
      out.write("          <div class=\"col-lg-3 col-md-6 col-sm-6\">\n");
      out.write("            <div class=\"card card-stats\">\n");
      out.write("              <div class=\"card-body \">\n");
      out.write("                <div class=\"row\">\n");
      out.write("                  <div class=\"col-5 col-md-4\">\n");
      out.write("                    <div class=\"icon-big text-center icon-warning\">\n");
      out.write("                      <i class=\"nc-icon nc-globe text-warning\"></i>\n");
      out.write("                    </div>\n");
      out.write("                  </div>\n");
      out.write("                  <div class=\"col-7 col-md-8\">\n");
      out.write("                    <div class=\"numbers\">\n");
      out.write("                      <p class=\"card-category\">Found Items</p>\n");
      out.write("                      <p class=\"card-title\">+10\n");
      out.write("                        <p>\n");
      out.write("                    </div>\n");
      out.write("                  </div>\n");
      out.write("                </div>\n");
      out.write("              </div>\n");
      out.write("              <div class=\"card-footer \">\n");
      out.write("                <hr>\n");
      out.write("                <div class=\"stats\">\n");
      out.write("                  <i class=\"fa fa-refresh\"></i> Update Now\n");
      out.write("                </div>\n");
      out.write("              </div>\n");
      out.write("            </div>\n");
      out.write("          </div>\n");
      out.write("            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n");
      out.write("          <div class=\"col-lg-3 col-md-6 col-sm-6\">\n");
      out.write("            <div class=\"card card-stats\">\n");
      out.write("              <div class=\"card-body \">\n");
      out.write("                <div class=\"row\">\n");
      out.write("                  <div class=\"col-5 col-md-4\">\n");
      out.write("                    <div class=\"icon-big text-center icon-warning\">\n");
      out.write("                      <i class=\"nc-icon nc-vector text-danger\"></i>\n");
      out.write("                    </div>\n");
      out.write("                  </div>\n");
      out.write("                  <div class=\"col-7 col-md-8\">\n");
      out.write("                    <div class=\"numbers\">\n");
      out.write("                      <p class=\"card-category\">Lost Items</p>\n");
      out.write("                      <p class=\"card-title\">+15\n");
      out.write("                        <p>\n");
      out.write("                    </div>\n");
      out.write("                  </div>\n");
      out.write("                </div>\n");
      out.write("              </div>\n");
      out.write("              <div class=\"card-footer \">\n");
      out.write("                <hr>\n");
      out.write("                <div class=\"stats\">\n");
      out.write("                  <i class=\"fa fa-clock-o\"></i> In the last hour\n");
      out.write("                </div>\n");
      out.write("              </div>\n");
      out.write("            </div>\n");
      out.write("          </div>\n");
      out.write("            \n");
      out.write("            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n");
      out.write("          <div class=\"col-lg-3 col-md-6 col-sm-6\">\n");
      out.write("            <div class=\"card card-stats\">\n");
      out.write("              <div class=\"card-body \">\n");
      out.write("                <div class=\"row\">\n");
      out.write("                  <div class=\"col-5 col-md-4\">\n");
      out.write("                    <div class=\"icon-big text-center icon-warning\">\n");
      out.write("                      <i class=\"nc-icon nc-favourite-28 text-primary\"></i>\n");
      out.write("                    </div>\n");
      out.write("                  </div>\n");
      out.write("                  <div class=\"col-7 col-md-8\">\n");
      out.write("                    <div class=\"numbers\">\n");
      out.write("                      <p class=\"card-category\">Tracked Items</p>\n");
      out.write("                      <p class=\"card-title\">+3\n");
      out.write("                        <p>\n");
      out.write("                    </div>\n");
      out.write("                  </div>\n");
      out.write("                </div>\n");
      out.write("              </div>\n");
      out.write("              <div class=\"card-footer \">\n");
      out.write("                <hr>\n");
      out.write("                <div class=\"stats\">\n");
      out.write("                  <i class=\"fa fa-refresh\"></i> Update now\n");
      out.write("                </div>\n");
      out.write("              </div>\n");
      out.write("            </div>\n");
      out.write("          </div>\n");
      out.write("        </div>\n");
      out.write("          \n");
      out.write("          \n");
      out.write("          <div class=\"row\">\n");
      out.write("          <div class=\"col-md-12\">\n");
      out.write("            <div class=\"card\">\n");
      out.write("              <div class=\"card-header\">\n");
      out.write("                <h4 class=\"card-title\">Posted Lost Items</h4>\n");
      out.write("                <p class=\"card-category\">Items that owners have lost and posted</p>\n");
      out.write("              </div>\n");
      out.write("              <div class=\"card-body\">\n");
      out.write("                <div class=\"table-responsive\">\n");
      out.write("                    ");

                        
                        Connection conn=null;
                        Statement statement = null;
                        ResultSet resultSet = null;

                        
      out.write("\n");
      out.write("                    \n");
      out.write("                  <table class=\"table\">\n");
      out.write("                    <thead class=\" text-primary\">\n");
      out.write("                    <th>\n");
      out.write("                        Property Id\n");
      out.write("                    </th>\n");
      out.write("                      <th>\n");
      out.write("                        Item Name\n");
      out.write("                      </th>\n");
      out.write("                      <th>\n");
      out.write("                        Place Lost\n");
      out.write("                      </th>\n");
      out.write("                      <th>\n");
      out.write("                        Date Lost\n");
      out.write("                      </th>                    \n");
      out.write("                      <!--<th class=\"text-right\">-->\n");
      out.write("                      <th>\n");
      out.write("                        Status\n");
      out.write("                      </th>\n");
      out.write("                    </thead>\n");
      out.write("                                   ");

                                       
                                try{
                                     ConnectionManager manager= new ConnectionManager();  
                                     conn = manager.getConnect();
                                statement=conn.createStatement();
                                String sql ="select * from lostitems";
                                       // + "WHERE status='Pending'";
                                resultSet = statement.executeQuery(sql);
                                while(resultSet.next()){
                                
      out.write("\n");
      out.write("                                \n");
      out.write("                                \n");
      out.write("                    <tbody>\n");
      out.write("                      <tr>\n");
      out.write("                          <td>\n");
      out.write("                          ");
      out.print(resultSet.getString("itemid") );
      out.write("\n");
      out.write("                        </td>\n");
      out.write("                        <td>\n");
      out.write("                          ");
      out.print(resultSet.getString("itemname") );
      out.write("\n");
      out.write("                        </td>\n");
      out.write("                        <td>\n");
      out.write("                         ");
      out.print(resultSet.getString("placelost") );
      out.write("\n");
      out.write("                        </td>\n");
      out.write("                        <td>\n");
      out.write("                         ");
      out.print(resultSet.getString("datelost") );
      out.write("\n");
      out.write("                        </td>\n");
      out.write("                         <td>\n");
      out.write("                         ");
      out.print(resultSet.getString("status") );
      out.write("\n");
      out.write("                        </td>\n");
      out.write("                      </tr>\n");
      out.write("                      ");

                                }
                                conn.close();
                                } catch (Exception e) {
                                e.printStackTrace();
                                }
                                
      out.write("\n");
      out.write("                    </tbody>\n");
      out.write("                  </table>\n");
      out.write("                </div>\n");
      out.write("              </div>\n");
      out.write("            </div>\n");
      out.write("          </div>\n");
      out.write("          <div class=\"col-md-12\">\n");
      out.write("            <div class=\"card card-plain\">\n");
      out.write("              <div class=\"card-header\">\n");
      out.write("                <h4 class=\"card-title\"> Posted Found Items</h4>\n");
      out.write("                <p class=\"card-category\">Items that have been found lost and posted</p>\n");
      out.write("              </div>\n");
      out.write("              <div class=\"card-body\">\n");
      out.write("                <div class=\"table-responsive\">\n");
      out.write("                  <table class=\"table\">\n");
      out.write("                    <thead class=\" text-primary\">\n");
      out.write("                        <th>\n");
      out.write("                        Property Id\n");
      out.write("                      </th>\n");
      out.write("                     <th>\n");
      out.write("                        Item Name\n");
      out.write("                      </th>\n");
      out.write("                      <th>\n");
      out.write("                        Place Found\n");
      out.write("                      </th>\n");
      out.write("                      <th>\n");
      out.write("                        Date Found\n");
      out.write("                      </th>\n");
      out.write("                     <th>\n");
      out.write("                      Status\n");
      out.write("                      </th>\n");
      out.write("                    </thead>\n");
      out.write("                            \n");
      out.write("                    ");

                                try{
                                     ConnectionManager manager= new ConnectionManager();  
                                     conn = manager.getConnect();
                                statement=conn.createStatement();
                                String sql1 ="select * from founditems";
                                      //  + " WHERE status='Pending'";
                                resultSet = statement.executeQuery(sql1);
                                while(resultSet.next()){
                                    
                                
      out.write("\n");
      out.write("                    <tbody>\n");
      out.write("                      <tr>\n");
      out.write("                          <td>\n");
      out.write("                          ");
      out.print(resultSet.getString("itemid") );
      out.write("\n");
      out.write("                        </td>\n");
      out.write("                        <td>\n");
      out.write("                          ");
      out.print(resultSet.getString("itemname") );
      out.write("\n");
      out.write("                        </td>\n");
      out.write("                        <td>\n");
      out.write("                         ");
      out.print(resultSet.getString("placefound") );
      out.write("\n");
      out.write("                        </td>\n");
      out.write("                        <td>\n");
      out.write("                         ");
      out.print(resultSet.getString("datefound") );
      out.write("\n");
      out.write("                        </td>\n");
      out.write("                       \n");
      out.write("                         <td>\n");
      out.write("                            ");
      out.print(resultSet.getString("status") );
      out.write("\n");
      out.write("                        </td>\n");
      out.write("                      </tr>\n");
      out.write("                       ");

                                }
                                conn.close();
                                } catch (Exception e) {
                                e.printStackTrace();
                                }
                                
      out.write("\n");
      out.write("                    </tbody>\n");
      out.write("                  </table>\n");
      out.write("                </div>\n");
      out.write("              </div>\n");
      out.write("            </div>\n");
      out.write("          </div>\n");
      out.write("              <div class=\"col-md-12\">\n");
      out.write("            <div class=\"card\">\n");
      out.write("              <div class=\"card-header\">\n");
      out.write("                <h4 class=\"card-title\">Tracked Items</h4>\n");
      out.write("                <p class=\"card-category\">Items that have been traced and taken by owners</p>\n");
      out.write("              </div>\n");
      out.write("              <div class=\"card-body\">\n");
      out.write("                <div class=\"table-responsive\">\n");
      out.write("                  <table class=\"table\">\n");
      out.write("                    <thead class=\" text-primary\">\n");
      out.write("                        \n");
      out.write("                      <th>\n");
      out.write("                        ID\n");
      out.write("                      </th>\n");
      out.write("                      <th>\n");
      out.write("                        Name\n");
      out.write("                      </th>\n");
      out.write("                      <th>\n");
      out.write("                        Description\n");
      out.write("                      </th>\n");
      out.write("                      <th>\n");
      out.write("                        Date Taken\n");
      out.write("                      </th>\n");
      out.write("                      <th>\n");
      out.write("                        Time Taken\n");
      out.write("                      </th>\n");
      out.write("                      <th>\n");
      out.write("                        More\n");
      out.write("                      </th>\n");
      out.write("                    </thead>\n");
      out.write("                    ");

                                try{
                                    
                                     ConnectionManager manager= new ConnectionManager();  
                                     conn = manager.getConnect();
                                statement=conn.createStatement();
                                String sql6 ="select * from tracked";
                                resultSet = statement.executeQuery(sql6);
                                    while(resultSet.next()){ 
                    
      out.write("\n");
      out.write("                    <tbody>\n");
      out.write("                      <tr>\n");
      out.write("                        <td>\n");
      out.write("                          ");
      out.print(resultSet.getString("itemid") );
      out.write("\n");
      out.write("                        </td>\n");
      out.write("                        <td>\n");
      out.write("                          ");
      out.print(resultSet.getString("itemname") );
      out.write("\n");
      out.write("                        </td>\n");
      out.write("                        <td>\n");
      out.write("                          ");
      out.print(resultSet.getString("description") );
      out.write("\n");
      out.write("                        </td>\n");
      out.write("                        \n");
      out.write("                        <td>\n");
      out.write("                          ");
      out.print(resultSet.getString("datetaken") );
      out.write("\n");
      out.write("                        </td>\n");
      out.write("                        <td>\n");
      out.write("                          ");
      out.print(resultSet.getString("timetaken") );
      out.write("\n");
      out.write("                        </td>\n");
      out.write("                        <td>\n");
      out.write("                            <a href=\"\">Details</a>\n");
      out.write("                        </td>\n");
      out.write("                      </tr>\n");
      out.write("                      ");

                                }
                                conn.close();
                                } catch (Exception e) {
                                e.printStackTrace();
                                }
                                
      out.write("\n");
      out.write("\n");
      out.write("                    </tbody>\n");
      out.write("                  </table>\n");
      out.write("                </div>\n");
      out.write("              </div>\n");
      out.write("            </div>\n");
      out.write("          </div>\n");
      out.write("        </div>\n");
      out.write("          \n");
      out.write("          \n");
      out.write("          \n");
      out.write("      </div>\n");
      out.write("      <footer class=\"footer footer-black  footer-white \">\n");
      out.write("        <div class=\"container-fluid\">\n");
      out.write("          <div class=\"row\">\n");
      out.write("            <nav class=\"footer-nav\">\n");
      out.write("              <ul>\n");
      out.write("                <li>\n");
      out.write("                  <a href=\"#\" target=\"_blank\">Efinder Systems</a>\n");
      out.write("                </li>\n");
      out.write("                <li>\n");
      out.write("                  <a href=\"#\" target=\"_blank\">Blog</a>\n");
      out.write("                </li>\n");
      out.write("                <li>\n");
      out.write("                  <a href=\"#\" target=\"_blank\">Licenses</a>\n");
      out.write("                </li>\n");
      out.write("              </ul>\n");
      out.write("            </nav>\n");
      out.write("            <div class=\"credits ml-auto\">\n");
      out.write("              <span class=\"copyright\">\n");
      out.write("                ©\n");
      out.write("                <script>\n");
      out.write("                  document.write(new Date().getFullYear())\n");
      out.write("                </script>, made with <i class=\"fa fa-heart heart\"></i> by Efinder Developers\n");
      out.write("              </span>\n");
      out.write("            </div>\n");
      out.write("          </div>\n");
      out.write("        </div>\n");
      out.write("      </footer>\n");
      out.write("    </div>\n");
      out.write("  </div>\n");
      out.write("  <!--   Core JS Files   -->\n");
      out.write("  <script src=\"../assets/js/core/jquery.min.js\"></script>\n");
      out.write("  <script src=\"../assets/js/core/popper.min.js\"></script>\n");
      out.write("  <script src=\"../assets/js/core/bootstrap.min.js\"></script>\n");
      out.write("  <script src=\"../assets/js/plugins/perfect-scrollbar.jquery.min.js\"></script>\n");
      out.write("  <!--  Google Maps Plugin    -->\n");
      out.write("  <script src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyD-EqDa-aTpuUS0d9A6VCx8pH-kM6C3Q2k\"></script>\n");
      out.write("  <!-- Chart JS -->\n");
      out.write("  <script src=\"../assets/js/plugins/chartjs.min.js\"></script>\n");
      out.write("  <!--  Notifications Plugin    -->\n");
      out.write("  <script src=\"../assets/js/plugins/bootstrap-notify.js\"></script>\n");
      out.write("  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->\n");
      out.write("  <script src=\"../assets/js/paper-dashboard.min.js?v=2.0.0\" type=\"text/javascript\"></script>\n");
      out.write("  <!-- Paper Dashboard DEMO methods, don't include it in your project! -->\n");
      out.write("  <script src=\"../assets/demo/demo.js\"></script>\n");
      out.write("  <script>\n");
      out.write("    $(document).ready(function() {\n");
      out.write("      // Javascript method's body can be found in assets/assets-for-demo/js/demo.js\n");
      out.write("      demo.initChartsPages();\n");
      out.write("    });\n");
      out.write("  </script>\n");
      out.write("</body>\n");
      out.write("\n");
      out.write("</html>\n");
      out.write("\n");
 
    }
    else{
         response.sendRedirect("http://localhost:8080/egertonfinder/errorpage.jsp?404 error! The page you are looking for could not be found");//404 error page will come here
     }
    
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
