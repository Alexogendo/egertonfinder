<%-- 
    Document   : loginunsuccessful
    Created on : May 30, 2019, 2:43:40 PM
    Author     : Alex
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page errorPage = "errorpage.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="google" content="notranslate"/>
	<meta name="description" content="Efinder Login | Sign into your Efinder account." />
	<meta name="robots" content="index, follow" />

	<title>Efinder Login</title>

	<link rel="stylesheet" href="content/css/fonts.min.css" />
	<link rel="stylesheet" href="content/css/min.css" />
	<link rel="stylesheet" href="content/css/login.min.css" />
	<script src="content/js/polyfills.min.js"></script>
	<script src="content/js/core-public.min.js"></script>
	<script src="content/js/mask.min.js"></script>
	<script src="content/js/login.min.js"></script>

	<link rel="shortcut icon" href="content/images/img_467397.png" />
        
<script> 
function validate()
{ 
 var Email = document.form.Email.value;
 var Password = document.form.Password.value;
  
 if (Email===null || Email==="")
 { 
 alert("Email field cannot be blank"); 
 return false; 
 }
 else if (Password===null || Password==="")
 { 
 alert("Password field cannot be blank"); 
 return false; 
 }
  else if(Password.length<6)
 { 
 alert("Password must be at least 6 characters long."); 
 return false; 
 } 
 } 
</script> 
<style>
.errormsg:hover{
text-transform: uppercase;
text-decoration: underline;
}
</style>
</head>
<body>
	<script id="CaspioConfig" type="text/json">
            {"caspioDomain":null,"releaseVersion":"17.##VersionPostfix##","forgotPwdURL":"https://id.caspio.com/forgotpassword","forgotLoginUrl":"","supportURL":"https://www.caspio.com/support/","gettingStartedURL":"https://www.caspio.com/get-started/","freeTrialUrl":"https://pages.caspio.com/free-trial","privacyURL":"https://www.caspio.com/caspio-privacy-statement/","termsOfUse":"https://www.caspio.com/terms-of-use-agreement/","termsOfServiceUrl":"https://www.caspio.com/caspio-bridge-terms-of-service/","systemRequirementsUrl":"https://howto.caspio.com/system-requirements/","imagePath":"/content/images","passwordSecurityURL":null,"changePasswordURL":null,"validatePasswordURL":null,"myProfileURL":null,"partnersPortalMenuLinkVisible":false,"partnersPortalMenuLinkUrl":null,"loginURL":null,"cloudLogOutURL":null,"dashboardURL":null,"signupURL":null,"signupProfileURL":null,"minLengthCustomSubdomainName":0,"maxLengthCustomSubdomainName":0,"passwordAjaxValidationDelay":0,"sessionExpiredURL":null,"logoutUrl":null,"sessionExpirationCountdownSeconds":0,"sessionExpirationSimplePingSeconds":0,"openSessionExpirationDialogRePingFrequency":0,"allow2FA":false,"showSendAgainTfaOtpTimeout":60,"tfaSmsAllowedCountries":null}
</script>
<script id="CaspioRequest" type="text/json">
	{"UserName":null,"Email":null}
</script>

<div class="cb_container">
	<div class="cb_body">
		<div class="left-side">

			<div id="TopAnnouncement" class="top-announcement"></div>
                        <div  style="text-align: center;"> <p style="color: red; font-style: bold; font-size: 16px">
                                Sorry, you are not a registered user! <br>Please sign up or contact us <a class="errormsg" href="#"><strong><u> <i>here</i></u>.</strong></a></p> </div>
			<div class="container-info">
				
<link rel="stylesheet" href="/content/less/caspio-logo.min.css" />

<div class="caspio-logo">
    <img src="content/images/img_467397.png" height="50" width="50" style="margin-left: 240px" alt="" /><span ><h3 style="margin-top: -40px; margin-left: 150px">Efinder</h3></span>
</div>
<br>
				<div id="first-login-step" class="box">
                                    <form  name="form" autocomplete="off" action="login" onsubmit="return validate()">
						<noscript>
							<div class="validation-message-error">
								<div class="icon"></div>
								<div class="message">
									<b>JavaScript must be enabled.</b>Please enable JavaScript in your browser to use Efinder System.
								</div>
							</div>
							<div class="form-row need-help">
								<span>Need Help?</span>
								<a href="#" target="_blank">Contact Support</a>
							</div>
						</noscript>
						<div id="SystemErrorContainer"></div>
						<div id="ErrorContainer"></div>
						<div class="form-row">
							<span class="label">Email</span>
                                                        <input id="EmailField" type="email" name="Email" class="field" tabindex="1" minlength="11" maxlength="60" autocomplete="off" required="required"/>
						</div>
						<div class="form-row">
							<span class="label">Password</span> <a class="forgot-password-link" href="http://localhost:8080/egertonfinder/forgotpassword/resetpassword.jsp" target="_blank">Forgot Your Password?</a>
                                                        <input id="PasswordField" type="password" name="Password" class="field" tabindex="2" autocomplete="off" minlength="6" maxlength="60" autocomplete="off" required="required"/>

						</div>

						<div class="form-row action">
							<input id="LoginButton" name="LoginButton" type="submit" value="Log In" class="button-primary large-wide" tabindex="4" />
						</div>
						<!--<input id="redirectPath" name="redirectPath" type="hidden" value="#" />-->
						<div>
							<input type="checkbox" id="RememberMe" class="" tabindex="3" />
							<label for="RememberMe">Remember me</label>
						</div>
					</form>
				</div>
				<div id="second-login-step" class="box hidden"></div>
				<div class="login-footer">
					<span>Don't Have Account & Not an Administrator?</span>
					<div class="try-free-button">
						<a href="http://localhost:8080/egertonfinder/clients/signup.jsp" target="_blank">Clients Can Sign Up Here.</a>
					</div>
				</div>
			</div>
		</div>


		<div class="right-side">
                    <h3 style="color: #ffffff">Egerton University Lost and Found System</h3>
                    <iframe class="fadeOut" id="promoFrame" src="content/html/banner.html"  scrolling="no"></iframe>
		</div>

		<script id="LoginConfigJsonModel" type="text/json">
		{"systemRequirementsUrl":"https://howto.caspio.com/system-requirements/"}
		</script>
	</div>
	<div class="cb_footer">
		
<link rel="stylesheet" href="content/css/footer.min.css" />

<div class="footer" style="">
	<span>&copy; 2019  Efinder, Inc.</span>
	<span class="additional-links">
		<a href="#" target="_blank">Privacy</a>
		<span>|</span>
		<a href="#" target="_blank">Terms</a>
	</span>
</div>
	</div>
</div>
</body>
</html>
