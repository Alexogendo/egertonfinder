/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    var callUrl = '/user/ajax/';
    function actionBtn(elementId, elementClass){
        $(elementId).click(function (e) {
            $(elementClass).toggleClass('hide');
            $(this).toggleClass('ion-ios-close').toggleClass('text-danger').toggleClass('ion-ios-plus').toggleClass('text-success');
        })
    }

    profileChars = {'#_academic': '._academic', '#_profession': '._profession', '#_oqs': '._oqs', '#_pms': '._pms', '#_eh': '._eh',
        '#_skill': '._skill','#_resume_doc': '._resume_doc', '#_ref': '._ref'};
    for (var elementId in profileChars){
        if (profileChars.hasOwnProperty(elementId)){
            actionBtn(elementId, profileChars[elementId]);
        }
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function getAction(data, callback) {
        $.ajax({
            url: callUrl,
            type:'GET',
            dataType: 'json',
            data: data,
            success: callback,
            error: function (error) {}
        });
    }
    // get resume douments
    var getResumeDocs = (function ResumeDocs() {
        data = {_resume_doc:''};
        function success(dataObj) {
            $('table.resdocs tr').remove();
            $.each(dataObj, function (obj) {
                data = dataObj[obj];
                var markup = "<tr><td>" +
                    "<a href='/uploads/" + data['resume_doc'] + "'>"+ (obj + 1 ) +" "+ data['description'] + "</a></td>" +
                    "<td class='text-right'><i data-value='"+ data['id'] +"' class='icon ion-ios-close h4 text-danger resume-del pull'></i></td></tr>";
                $('table.resdocs').append(markup);
            })
        }
        getAction(data, success);
        return ResumeDocs
    })();

    // get academic qualification
    // use a named self-invoking function
    var getAcademics = (function Academics() {
        data = {_academic: ''};
        function success(dataObj) {
            // empty table
            $('table.academic tbody tr').remove();
            // loop through the data object and get individual qualification
            $.each(dataObj, function (obj) {
                data = dataObj[obj];
                if (data['end_date']===null) end_date = 'Present'; else end_date = data['end_date'];
                // put the qualification into a markup
                var markup = "<tr>" +
                    "<td>"+ data['academic_qualification__qualification_name'] + "</td>" +
                    "<td>"+ data['field'] + "</td>" +
                    "<td>"+ data['institution'] + "</td>" +
                    "<td>"+ data['start_date'] + "</td>" +
                    "<td>"+ end_date + "</td>" +
                    "<td><i data-value='"+ data['id'] +"' class='icon ion-ios-close h4 text-danger academic-del'></i></td>" +
                    "</tr>";
                // append to the relevant table
                $('table.academic tbody').append(markup);
            })
        }
        getAction(data, success);
        return Academics
    })();

    // get professional Qualification
    var getProfessions = (function Professions() {
        data = {_profession: ''};
        function success(dataObj) {
            // empty table
            $('table.profession tbody tr').remove();
            // loop through the data object and get individual qualification
            $.each(dataObj, function (obj) {
                data = dataObj[obj];
                if (data['end_date']===null) end_date = 'Present'; else end_date = data['end_date'];
                // put the qualification into a markup
                var markup = "<tr>" +
                    "<td>"+ data['professional_qualification__qualification_name'] + "</td>" +
                    "<td>"+ data['field'] + "</td>" +
                    "<td>"+ data['institution'] + "</td>" +
                    "<td>"+ data['start_date'] + "</td>" +
                    "<td>"+ end_date + "</td>" +
                    "<td><i data-value='"+ data['id'] +"' class='icon ion-ios-close h4 text-danger profession-del'></i></td>" +
                    "</tr>";
                // append to the relevant table
                $('table.profession tbody').append(markup);
            })
        }
        getAction(data, success);
        return Professions
    })();

    // get other trainings and qualifications
    var getOtherQs = (function OtherQs() {
        data = {_oqs: ''};
        function success(dataObj) {
            // empty table
            $('table.oqs tbody tr').remove();
            // loop through the data object and get individual qualification
            $.each(dataObj, function (obj) {
                data = dataObj[obj];
                // put the qualification into a markup
                var markup = "<tr>" +
                    "<td style=\"word-wrap: break-word;min-width: 160px;max-width: 160px;\">"+ data['qualification'] + "</td>" +
                    "<td style=\"word-wrap: break-word;min-width: 160px;max-width: 160px;\">"+ data['institution'] + "</td>" +
                    "<td style=\"word-wrap: break-word;min-width: 160px;max-width: 300px;\">"+ data['details'] +"</td>" +
                    "<td><i data-value='"+ data['id'] +"' class='icon ion-ios-close h4 text-danger oqs-del'></i></td>" +
                    "</tr>";
                // append to the relevant table
                $('table.oqs tbody').append(markup);
            })
        }
        getAction(data, success);
        return OtherQs
    })();

    // get professional memberships
    var getPms = (function Pms() {
        data = {_pm: ''};
        function success(dataObj) {
            // empty table
            $('table.pms tbody tr').remove();
            $.each(dataObj, function (obj) {
                data = dataObj[obj];
                // put the qualification into a markup
                var markup = "<tr>" +
                    "<td>"+ data['institution'] + "</td>" +
                    "<td>"+ data['membership_type'] + "</td>" +
                    "<td>"+ data['membership_number'] +"</td>" +
                    "<td><i data-value='"+ data['id'] +"' class='icon ion-ios-close h4 text-danger pms-del'></i></td>" +
                    "</tr>";
                // append to the relevant table
                $('table.pms tbody').append(markup);
            })
        }
        getAction(data, success);
        return Pms
    })();

    //get employment history
    var getEH = (function eH() {
        data = {_eh: ''};
        function success(dataObj) {
            //empty table
            $('table.eh tbody tr').remove();
            //populate table
            $.each(dataObj, function (obj) {
                data = dataObj[obj];
                if (data['end_date']===null) end_date = 'Present'; else end_date = data['end_date'];
                // put the qualification into a markup
                var markup = "<tr>" +
                    "<td>"+ data['employer'] + "</td>" +
                    "<td>"+ data['position'] + "</td>" +
                    "<td>"+ data['start_date'] + "</td>" +
                    "<td>"+ end_date + "</td>" +
                    "<td>"+ data['gross_salary'] + "</td>" +
                    "<td><i data-value='"+ data['id'] +"' class='icon ion-ios-close h4 text-danger eh-del'></i></td>" +
                    "</tr>";
                // append to the relevant table
                $('table.eh tbody').append(markup);
            })
        }
        getAction(data, success);
        return eH
    })();

    // get skills
    var getSkill = (function Skill() {
        data = {_skill: ''};
        function success(dataObj) {
            $('table.skill tbody tr').remove();
            $.each(dataObj, function (obj) {
                data = dataObj[obj];
                var markup = "<tr>" +
                    "<td>"+ data['skill'] + "</td>" +
                    "<td style=\"word-wrap: break-word;min-width: 160px;max-width: 400px;\">"+ data['details'] + "</td>" +
                    "<td><i data-value='"+ data['id'] +"' class='icon ion-ios-close h4 text-danger skill-del'></i></td>" +
                    "</tr>";
                $('table.skill tbody').append(markup);
            })
        }
        getAction(data, success);
        return Skill
    })();

    // get referees
    var getRefs = (function Refs() {
        data = {_ref: ''};
        function success(dataObj) {
            $('table.ref tbody tr').remove();
            $.each(dataObj, function (obj) {
                data = dataObj[obj];
                var markup = "<tr>" +
                    "<td>" + data['referee_name'] + "</td>" +
                    "<td>" + data['referee_address'] + "</td>" +
                    "<td>" + data['referee_phone'] + "</td>" +
                    "<td>" + data['referee_email'] + "</td>" +
                    "<td>" + data['relationship'] + "</td>" +
                    "<td>" + data['years_known'] + "</td>" +
                    "<td>" + data['referee_occupation'] + "</td>" +
                    "<td><i data-value='" + data['id'] + "' class='icon ion-ios-close h4 text-danger ref-del'></i></td>" +
                    "</tr>";
                $('table.ref tbody').append(markup)
            })
        }
        getAction(data, success);
        return Refs
    })();
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // update profile details
    $('._profile').on("submit", function (e) {
        e.preventDefault();
        data = {
            first_name:$('form._profile #id_first_name').val(),
            last_name:$('form._profile #id_last_name').val(),
            other_name:$('form._profile #id_other_name').val(),
            gender:$('form._profile #id_gender').val(),
            nationality:$('form._profile #id_nationality').val(),
            constituency:$('form._profile #id_constituency').val(),
            home_county:$('form._profile #id_home_county').val(),
            residence_county:$('form._profile #id_residence_county').val(),
            primary_phone:$('form._profile #id_primary_phone').val(),
            alt_phone:$('form._profile #id_alt_phone').val(),
            alt_email:$('form._profile #id_alt_email').val(),
            post_box:$('form._profile #id_post_box').val(),
            postal_code:$('form._profile #id_postal_code').val(),
            town:$('form._profile #id_town').val(),
            ncpwd_number:$('form._profile #id_ncpwd_number').val(),
            _profile:''
        };
        $.ajax({url:callUrl, type:'POST', data:data, success: function (data) {$.notify({ icon: 'ti-user', message: data['success_msg']}, { type: 'success', timer: 5000 });}, error: function (error) {}})
    });


    function addProfileItem(data, refreshData, formClass) {
        $.ajax({
            url: callUrl,
            type:'POST',
            data: data,
            success: function (data) {
                // success notification
                $.notify({ icon: 'ti-medall', message: data['success_msg']}, { type: 'success', timer: 5000 });
                // refresh the academics data
                refreshData();
                // reset the form
                $(formClass).trigger('reset');
            },error: function (data) {}
        });
    }

    //updata referees
    $('._ref').on('submit', function (e) {
       e.preventDefault();
       data = {
           name: $('form._ref #id_referee_name').val(),
           address: $('form._ref #id_referee_address').val(),
           phone: $('form._ref #id_referee_phone').val(),
           email: $('form._ref #id_referee_email').val(),
           relationship: $('form._ref #id_relationship').val(),
           years_known: $('form._ref #id_years_known').val(),
           occupation: $('form._ref #id_referee_occupation').val(),
           _ref: ''
       };
       addProfileItem(data, getRefs, '._ref')
    });

    // add academic qualification
    $('._academic').on("submit", function (e) {
        e.preventDefault();
        data = {
            academic_qualification: $('form._academic #id_academic_qualification').val(),
            field: $('form._academic #id_field').val(),
            institution: $('form._academic #id_institution').val(),
            start_date: $('form._academic #id_start_date').val(),
            end_date: $('form._academic #id_end_date').val(),
            _academic: ''
        };
        addProfileItem(data, getAcademics, '._academic')
    });

    // add professional qualification
    $('._profession').on("submit", function (e) {
        e.preventDefault();
        data = {
            professional_qualification: $('form._profession #id_professional_qualification').val(),
            field: $('form._profession #id_field').val(),
            institution: $('form._profession #id_institution').val(),
            start_date: $('form._profession #id_start_date').val(),
            end_date: $('form._profession #id_end_date').val(),
            _profession: ''
        };
        addProfileItem(data, getProfessions, '._profession')
    });

    // add other qualifications and trainings
    $('._oqs').on("submit", function (e) {
        e.preventDefault();
        data =  {
            qualification: $('form._oqs #id_qualification').val(),
            institution: $('form._oqs #id_institution').val(),
            details: $('form._oqs #id_details').val(),
            _oqs: ''
        };
        addProfileItem(data, getOtherQs, '._oqs')
    });

    // add professional memberships
    $('._pms').on("submit", function (e) {
        e.preventDefault();
        data =  {
            institution: $('form._pms #id_institution').val(),
            membership_type: $('form._pms #id_membership_type').val(),
            membership_number: $('form._pms #id_membership_number').val(),
            _pm: ''
        };
        addProfileItem(data, getPms, '._pms')
    });

    // add employment history
    $('._eh').on("submit", function (e) {
        e.preventDefault();
        data =  {
            employer: $('form._eh #id_employer').val(),
            position: $('form._eh #id_position').val(),
            start_date: $('form._eh #id_start_date').val(),
            end_date: $('form._eh #id_end_date').val(),
            gross_salary: $('form._eh #id_gross_salary').val(),
            _eh: ''
        };
        addProfileItem(data, getEH, '._eh')
    });

    //add skills
    $('._skill').on("submit", function (e) {
        e.preventDefault();
        data =  {
            skill: $('form._skill #id_skill').val(),
            details: $('form._skill #id_details').val(),
            _skill: ''
        };
        addProfileItem(data, getSkill, '._skill')
    });
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function delAction(data, dataRefresh) {
        $.ajax({
            url: callUrl,
            type:'POST',
            data: data,
            success: function (data) {
                // success notification
                $.notify({ icon: 'ti-info-alt', message: data['success_msg']},{type: 'danger', timer: 5000});
                // refresh the table
                dataRefresh();
            }
        })
    }

    // delete academic qualification
    function delAcademics(aq) {
        data = {aq: aq, _del_aq: ''};
        delAction(data, getAcademics);
    }

    // delete professional qualification
    function delProfessions(pq) {
        data = {pq: pq, _del_pq: ''};
        delAction(data, getProfessions);
    }

    //delete other qs & trainings
    function delOtherQs(oq) {
        data = {oq: oq,  _del_oq: '' };
        delAction(data, getOtherQs);
    }

    // delete professional memberships
    function delPms(pm) {
        data = {pm: pm, _del_pm: ''};
        delAction(data, getPms);
    }

    // delete employment history
    function delEH(eh) {
        data = {eh: eh, _del_eh: ''};
        delAction(data, getEH);
    }

    // delete skills
    function delSkill(skill) {
        data = {skill: skill, _del_skill: ''};
        delAction(data, getSkill);
    }

    //delete resume document
    function delResumeDoc(id) {
        data = {id:id, _del_resume: ''};
        delAction(data, getResumeDocs);
    }
    //delete referee
    function delRef(id) {
        data = {id:id, _del_ref: ''};
        delAction(data, getRefs);
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // tie the event to document since its loaded asychronously
    function delActionCall(callClass, delClass) {
        $(document).on('click', callClass, function (e) {
            // get id of item and pass it to the delete function
            delClass($(this).attr('data-value'));
        });
    }

    delCallClasses = {'.academic-del': delAcademics, '.profession-del': delProfessions, '.ops-del': delOtherQs, '.pms-del': delPms,
        '.eh-del': delEH, '.skill-del': delSkill, '.resume-del': delResumeDoc, '.ref-del': delRef};
    for (var callClass in delCallClasses){
        if (delCallClasses.hasOwnProperty(callClass)){
            delActionCall(callClass, delCallClasses[callClass])
        }
    }
    /*$(document).on('click', '.academic-del', function (e) {
        // get id of item and pass it to the delete function
        delAcademics($(this).attr('data-value'));
    });
    $(document).on('click', '.profession-del', function (e) {
        // get id of item and pass it to the delete function
        delProfessions($(this).attr('data-value'));
    });
    $(document).on('click', '.oqs-del', function (e) {
        // get id of item and pass it to the delete function
        delOtherQs($(this).attr('data-value'));
    })*/
});
