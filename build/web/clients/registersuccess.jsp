<%-- 
    Document   : typography
    Created on : May 28, 2019, 12:48:22 PM
    Author     : Alex

onload="demo.showNotification14('top','center')"
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../content/images/img_467397.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../content/images/img_467397.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Egerton Lost and Lost Found Systems
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  <!-- CSS Files -->
  <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="../assets/css/paper-dashboard.css?v=2.0.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="../assets/demo/demo.css" rel="stylesheet" />
  
  <style>
     #onhoverLogout{
                visibility: hidden;
            }
            #onlogoutIcon:hover ~ #onhoverLogout{
                visibility: visible;
            }
    </style>
</head>

<body class="" style="background-color: whitesmoke" onload="demo.showNotification13('top','center')">
   
    <div class="main-panel" style="background-color: whitesmoke">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-toggle">
              <button type="button" class="navbar-toggler">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </button>
            </div>
              <marquee direction="right"> <a class="navbar-brand" href="#">Efinder (Egerton Lost and Found Systems)</a></marquee>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end" id="navigation">
              <h8>Have an account? Login <a href="http://localhost:8080/egertonfinder/" style="color: blue; font-style: italic">here</a></h8>

          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <!-- <div class="panel-header panel-header-sm">
  
  onload="demo.showNotification13('top','center')"
</div> -->
      <div class="content">
        
                        <div class="row" style="margin-left: 100px">
    
          <div class="col-md-8">
            <div class="card card-user">
              <div class="card-header">
                <h5 class="card-title"style="text-align: center">Clients Registration Form</h5>
              </div>
              <div class="card-body">
                  <form action="register.jsp" method="post">
                  <div class="row">
                    <div class="col-md-5 pr-1">
                      <div class="form-group">
                        <label>Account Name (disabled)</label>
                        <input type="text" class="form-control" disabled="" placeholder="Company" value="Efinder Systems">
                      </div>
                    </div>
                    <div class="col-md-3 px-1">
                      <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" placeholder="Password" name="tempass" autocomplete="off" minlength="6" maxlength="20" required="required">
                      </div>
                    </div>
                    <div class="col-md-4 pl-1">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" class="form-control" placeholder="Email" name="email" autocomplete="off" minlength="11" maxlength="60" required="required">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6 pr-1">
                      <div class="form-group">
                        <label>First Name</label>
                        <input type="text" class="form-control" placeholder="First Name" name="firstname" autocomplete="off" minlength="3" maxlength="20" required="required">
                      </div>
                    </div>
                    <div class="col-md-6 pl-1">
                      <div class="form-group">
                        <label>Last Name</label>
                        <input type="text" class="form-control" placeholder="Last Name" name="lastname" autocomplete="off" minlength="3" maxlength="20" required="required">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6 pr-1">
                      <div class="form-group">
                        <label>Phone Number</label>
                        <input type="text" class="form-control" placeholder="07xxxxxxxx" name="phone" autocomplete="off" minlength="10" maxlength="10" required="required">
                      </div>
                    </div>
                      
                      <div class="col-md-6 pr-1">
                      <div class="form-group">
                        <label>National ID</label>
                        <input type="text" class="form-control" placeholder="ID Number" name="nid" autocomplete="off" minlength="8" maxlength="8" required="required">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6 pr-1">
                      <div class="form-group">
                        <label>Role</label>
                        <select type="text" class="form-control" name="role" required="required">
                            <option value="Student" selected>Student</option>
                            <option value="Staff">Staff</option>
                            <option value="Surbodinate">Security</option>
                            <option value="Surbodinate">Subordinate</option>
                            
                        </select>
                      </div>
                    </div>
                    <div class="col-md-6 pr-1">
                      <div class="form-group">
                        <label>Department</label>
                        <input type="text" class="form-control" placeholder="Department" name="department" autocomplete="off" minlength="5" maxlength="50" required="required">
                      </div>
                    </div>
<!--                    <div class="col-md-3 px-1">
                      <div class="form-group">
                          <label>Passport Photo</label><br>
                          <input type="file" name="fileupload" value="fileupload" id="fileupload" required="required">
                        <a style="text-decoration: underline; font-style: italic"> Upload here.</a>
               </div>
                    </div>-->
                  </div>
                      <div class="row">
                      <div class="col-md-6 pr-1">
                      <div class="form-group">
                        <label>Your Photo</label>
                        <input type="file" class="form-control"  name="image" autocomplete="off">
                         <a style="text-decoration: underline; font-style: bold; color: #0069d9"> Click here to upload</a>
                      </div>
                    </div>
                          
                  </div>
<br><br>
                  <div class="row">
                    <div class="update ml-auto mr-auto">
                      <button type="submit" class="btn btn-primary btn-round">Submit Registration</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        
        <hr>
        
          
      <footer class="footer footer-black  footer-white ">
        <div class="container-fluid">
          <div class="row">
            <nav class="footer-nav">
              <ul>
                <li>
                  <a href="#" target="_blank">Efinder</a>
                </li>
                <li>
                  <a href="#" target="_blank">Blog</a>
                </li>
                <li>
                  <a href="#" target="_blank">Licenses</a>
                </li>
              </ul>
            </nav>
            <div class="credits ml-auto">
              <span class="copyright">
                ©
                <script>
                  document.write(new Date().getFullYear())
                </script>, made with <i class="fa fa-heart heart"></i> by Efinder Developers
              </span>
            </div>
          </div>
        </div>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <script src="../assets/js/core/jquery.min.js"></script>
  <script src="../assets/js/core/popper.min.js"></script>
  <script src="../assets/js/core/bootstrap.min.js"></script>
  <script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD-EqDa-aTpuUS0d9A6VCx8pH-kM6C3Q2k"></script>
  <!-- Chart JS -->
  <script src="../assets/js/plugins/chartjs.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="../assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="../assets/js/paper-dashboard.min.js?v=2.0.0" type="text/javascript"></script>
  <!-- Paper Dashboard DEMO methods, don't include it in your project! -->
  <script src="../assets/demo/demo.js"></script>
</body>

</html>