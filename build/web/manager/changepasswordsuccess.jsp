<%-- 
    Document   : changepasswordsuccess
    Created on : May 28, 2019, 11:15:52 AM
    Author     : Alex
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../content/images/img_467397.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../content/images/img_467397.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Egerton Lost and Lost Found Systems
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  <!-- CSS Files -->
  <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="../assets/css/paper-dashboard.css?v=2.0.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="../assets/demo/demo.css" rel="stylesheet" />
  
  <style>
     #onhoverLogout{
                visibility: hidden;
            }
            #onlogoutIcon:hover ~ #onhoverLogout{
                visibility: visible;
            }
           
    </style>
</head>

<body class="" onload="demo.showNotification15('top','center')">
  <div class="wrapper ">
    <div class="sidebar" data-color="green" data-active-color="danger">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
    -->
      <div class="logo">
        <a href="http://www.creative-tim.com" class="simple-text logo-mini">
          <div class="logo-image-small">
            <img src="../content/images/img_467397.png">
          </div>
        </a>
        <a href="http://www.creative-tim.com" class="simple-text logo-normal">
          Efinder
          <!-- <div class="logo-image-big">
            <img src="../assets/img/logo-big.png">
          </div> -->
        </a>
      </div>
      <div class="sidebar-wrapper">
         <ul class="nav" style="">
          <li class="hove">
            <a href="./dashboard.jsp">
              <i class="nc-icon nc-bank"></i>
              <p>Dashboard</p>
            </a>
          </li>
          
          <li class="hove">
            <a href="./postfounditem.jsp">
              <i class="nc-icon nc-sound-wave"></i>
              <p>Post Found Item</p>
            </a>
          </li>
          <li class="hove">
            <a href="./postlostitem.jsp">
              <i class="nc-icon nc-sun-fog-29"></i>
              <p>Post Lost Item</p>
            </a>
          </li>
          <li class="hove">
            <a href="./assignitems.jsp">
              <i class="nc-icon nc-bulb-63"></i>
              <p>Assign Items to Owners</p>
            </a>
          </li>
          <li class="hove">
            <a href="./entries.jsp">
              <i class="nc-icon nc-tile-56"></i>
              <p>System Entries</p>
            </a>
          </li>
          <li class="hove">
            <a href="./printreports.jsp">
              <i class="nc-icon nc-paper"></i>
              <p>Print Reports</p>
            </a>
          </li>
          <li class="hove">
            <a href="./registeradmin.jsp">
              <i class="nc-icon nc-bullet-list-67"></i>
              <p>Register Admin</p>
            </a>
          </li>
          <li class="hove">
            <a href="./registerclients&pcs.jsp">
              <i class="nc-icon nc-single-copy-04"></i>
              <p>Register Clients & Pc's</p>
            </a>
          </li>
          <li class="hove">
            <a href="./user.jsp">
              <i class="nc-icon nc-single-02"></i>
              <p>User Profile</p>
            </a>
          </li>
          
          <li class="hove">
            <a href="http://localhost:8080/egertonfinder/Help/index.html">
              <i class="nc-icon nc-spaceship"></i>
              <p>Gain Experience</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-toggle">
              <button type="button" class="navbar-toggler">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </button>
            </div>
            <a class="navbar-brand" href="#">Egerton Lost and Found Systems</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end" id="navigation">
            <form>
              <div class="input-group no-border">
                <input type="text" value="" class="form-control" placeholder="Search...">
                <div class="input-group-append">
                  <div class="input-group-text">
                    <i class="nc-icon nc-zoom-split"></i>
                  </div>
                </div>
              </div>
            </form>
            <ul class="navbar-nav">
<!--              <li class="nav-item">
                <a class="nav-link btn-magnify" href="#pablo">
                  <i class="nc-icon nc-layout-11"></i>
                  <p>
                    <span class="d-lg-none d-md-block">Stats</span>
                  </p>
                </a>
              </li>-->
              <li class="nav-item btn-rotate dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="nc-icon nc-bell-55"></i>
                  <p>
                    <span class="d-lg-none d-md-block">Some Actions</span>
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="./notifications.jsp">Notifications</a>
                  <a class="dropdown-item" href="./user.jsp">Your Profile</a>
                  <a class="dropdown-item" href="./changepassword.jsp">Change Password</a>
                </div>
              </li>
              <li class="nav-item">
                <a class="nav-link btn-rotate" href="../logout">
                    <div id="onlogoutIcon">
                  <i class="nc-icon nc-button-power"></i>
                    </div>
                  <div id="onhoverLogout">Logout</div>
                  
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <!-- <div class="panel-header panel-header-sm">
  
  
</div> -->
  <div class="row" style="margin-left: 200px; margin-top: 150px">
    
            <script>
          function validateForm(){
              var x = document.myform.oldpass.value;
              var y1 = document.myform.newpass.value;
              var y2 = document.myform.conpass.value;
              if(x == null){
                  alert("Old password field should not be blank!");
                  return false;
              }
              else if(y1 == null){
                  alert("New password field should not be blank!");
                  return false;
              }
              else if(y2 == null){
                  alert("Confirm password field should not be blank!");
                  return false;
              }
              else (y1 !== y2){
                  alert("New password and confirm password do not match. Try again!");
                  return false;
              }
              
          }
          </script>
          <div class="col-md-8">
            <div class="card card-user">
              <div class="card-header">
                <h5 class="card-title"style="text-align: center">Change Password Form</h5>
              </div>
                <div class="card-body">
                  <form action="changepass.jsp" method="post" name="myform" onsubmit="return validateForm();">
                  
                  <div class="row">
                    <div class="col-md-6 pr-1">
                      <div class="form-group">
                        <label>Old Password</label>
                        <input type="password" class="form-control" placeholder="" name="oldpass" autocomplete="off" minlength="8" maxlength="190" required="required">
                      </div>
                    </div>
                  </div>
                      <div class="row">
                    <div class="col-md-6 pr-1">
                      <div class="form-group">
                        <label>New Password</label>
                        <input type="password" class="form-control" placeholder="" name="newpass" autocomplete="off" minlength="8" maxlength="190" required="required">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6 pr-1">
                      <div class="form-group">
                        <label>Confirm New Password</label>
                        <input type="password" class="form-control" placeholder="" name="conpass" autocomplete="off" minlength="8" maxlength="190" required="required">
                      </div>
                    </div>
                      
<!--                      <div class="col-md-6 pr-1">
                      <div class="form-group">
                        <label>National ID</label>
                        <input type="text" class="form-control" placeholder="ID Number" name="nid" autocomplete="off" minlength="8" maxlength="8" required="required">
                      </div>
                    </div>-->
                  </div>
                      
<br><br>
                  <div class="row">
                    <div class="update ml-auto mr-auto">
                      <button type="submit" class="btn btn-primary btn-round" onclick="return validateForm();">Submit</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        
        <hr>
      <footer class="footer footer-black  footer-white ">
        <div class="container-fluid">
          <div class="row">
            <nav class="footer-nav">
              <ul>
                <li>
                  <a href="#" target="_blank">Efinder</a>
                </li>
                <li>
                  <a href="#" target="_blank">Blog</a>
                </li>
                <li>
                  <a href="#" target="_blank">Licenses</a>
                </li>
              </ul>
            </nav>
            <div class="credits ml-auto">
              <span class="copyright">
                ©
                <script>
                  document.write(new Date().getFullYear())
                </script>, made with <i class="fa fa-heart heart"></i> by Efinder Developers
              </span>
            </div>
          </div>
        </div>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <script src="../assets/js/core/jquery.min.js"></script>
  <script src="../assets/js/core/popper.min.js"></script>
  <script src="../assets/js/core/bootstrap.min.js"></script>
  <script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD-EqDa-aTpuUS0d9A6VCx8pH-kM6C3Q2k"></script>
  <!-- Chart JS -->
  <script src="../assets/js/plugins/chartjs.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="../assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="../assets/js/paper-dashboard.min.js?v=2.0.0" type="text/javascript"></script>
  <!-- Paper Dashboard DEMO methods, don't include it in your project! -->
  <script src="../assets/demo/demo.js"></script>
</body>

</html>