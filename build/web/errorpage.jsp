<%-- 
    Document   : errorpage
    Created on : Jun 4, 2019, 9:53:26 AM
    Author     : Alex
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page isErrorPage="true" %>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html class="gr__colorlib_com" lang="en"><head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

	<title>Error 404! The page requested is not available</title>

	<!-- Google font -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,900" rel="stylesheet">

	<!-- Custom stlylesheet -->
	<link type="text/css" rel="stylesheet" href="content/css/style.css">

	<style>
            marquee {
        width: 100%;
        padding: 2px 0;
        background-color: #006699;
      }
            
            </style>

</head>

<body data-gr-c-s-loaded="true">
    <div id="animatingnav">
        <marquee direction="right"> <h2 style="color: #ffffff">Egerton University Lost And Found Systems.</h2></marquee>
    </div>
	<div id="notfound">
		<div class="notfound">
			<div class="">
                            <img src="content/images/oops.jpg" alt="O0ps" height="200" width="200"> 
			</div>
			<h2>404 - Page not found !!</h2>
			<p>The page you are looking for might have been removed, had its name changed or is temporarily unavailable. We are sorry for any inconveniences.</p>
			<a href="home">Go To Homepage</a>
		</div>
	</div>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script type="text/javascript" async="" src="https://www.google-analytics.com/analytics.js">
<script type="text/javascript" async="" src="content/js/analytics.js">
</script><script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13" type="text/javascript"></script>
<script type="text/javascript">
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>



</body><!-- This templates was made by Colorlib (https://colorlib.com) --></html>