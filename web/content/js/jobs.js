/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    var callUrl = '/user/ajax/';
    var getJobsApplied = (function JobsApplied() {
        $.ajax({
            url: callUrl, data: {_get_applied: ''}, type:'GET', dataType:'JSON',
            success: function (dataObject) {
                $("#jobs-applied li").remove();
                $("#jobs-applied hr").remove();
                $.each(dataObject, function (obj) {
                    data = dataObject[obj];
                    var markup = "<hr><li><a href='/user/jobs/"+ data['job__id'] +"'>" + data['job__job_title'] + "<br />" +
                        "<span class=\"text-muted\"><small>Ref:"+ data['job__ref_id'] +"</small></span></a>" +
                        "<span class='pull-right'><i data-value='"+ data['job__ref_id'] +"' class='icon ion-ios-close text-danger h4 del-job'></i></span></li>";
                    $("#jobs-applied").append(markup);
                })
            }
        });
        return JobsApplied
    })();

    $('#apply-job').click(function () { // job without any extra fields
        data = {ref_id: $(this).attr('data-value'), _job_apply: ''};
        function callback(data) {
            $.notify({ icon: 'ti-info-alt', message: data['success_msg']},{type: 'success', timer: 5000});
            window.location.reload();
        }
        $.ajax({url:callUrl,type: 'POST', data: data, dataType: 'JSON', success: callback})
    });

    $('#extra_field_job').submit(function (e) { // job with extra fields form
        e.preventDefault(); // preveent default form action
        ref_id = $('#extra_field_btn').attr('data-value');
        // get the form data
        $data = {'job_id': ref_id, '_extra_field_job': true};
        $formData = $(this).serializeArray();
        $.each($formData, function (k, value) {
            $data[value.name] = value.value
        });
        function callback(data) {
            $.notify({ icon: 'ti-info-alt', message: data['success_msg']},{type: 'success', timer: 5000});
            window.location.reload();
        }
        $.ajax({url: callUrl, type: 'POST', data: $data, success: callback})

    });

    $(document).on('click', '.del-job', function () {
        data = {ref_id: $(this).attr('data-value'), _del_applied: ''};
        function callback(data) {
            $.notify({ icon: 'ti-info-alt', message: data['success_msg']},{type: data['msg_type'], timer: 5000});
            window.location.reload();        
        }
        $.ajax({url:callUrl,type: 'POST', data: data, dataType: 'JSON', success: callback})
    })
});
