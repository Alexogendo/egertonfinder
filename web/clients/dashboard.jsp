<%-- 
    Document   : dashboard
    Created on : May 27, 2019, 12:12:01 AM
    Author     : Alex
--%>
  

<%@page contentType="text/html" pageEncoding="UTF-8" import="admins.AdminBean" %>  
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="admins.ConnectionManager"%>

<% 
                
                  //session.setMaxInactiveInterval(1800);
               AdminBean currentClient = ((AdminBean)(session.getAttribute("currentSessionClient")));%>
                   
               
               <%
                   String cid = currentClient.getId(); 
                   String cfname = currentClient.getFirstname();
                   String clname = currentClient.getLastname();
                   String cmail = currentClient.getEmail();
                   String cphone = currentClient.getPhone();
                   String cnational = currentClient.getNationalId();
                   String crole = currentClient.getRole();
                   
                   //int mId = Integer.parseInt(id);
                   
                   session.setAttribute("cid", cid);
                   session.setAttribute("cfname", cfname);
                   session.setAttribute("clname", clname);
                   session.setAttribute("cmail", cmail);
                   session.setAttribute("cphone", cphone);
                   session.setAttribute("cnational", cnational);
                   session.setAttribute("crole", crole);   
                   
if (crole.equals("Security") || crole.equals("Staff") || crole.equals("Student") || crole.equals("Subordinate")){
                   %>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../content/images/img_467397.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../content/images/img_467397.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Client Logged Successfully 
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  <!-- CSS Files -->
  <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="../assets/css/paper-dashboard.css?v=2.0.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="../assets/demo/demo.css" rel="stylesheet" />
  
  <style>
     #onhoverLogout{
                visibility: hidden;
            }
            #onlogoutIcon:hover ~ #onhoverLogout{
                visibility: visible;
            }
    </style>
</head>

<body class="" onload="demo.showNotification('top','center')">
  
    
  <div class="wrapper ">
    <div class="sidebar" data-color="green" data-active-color="danger">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
    -->
      <div class="logo">
        <a href="http://www.creative-tim.com" class="simple-text logo-mini">
          <div class="logo-image-small">
            <img src="../content/images/img_467397.png">
          </div>
        </a>
        <a href="http://www.creative-tim.com" class="simple-text logo-normal">
         Efinder
          <!-- <div class="logo-image-big">
            <img src="../assets/img/logo-big.png">
          </div> -->
        </a>
      </div>
      <div class="sidebar-wrapper">
         <ul class="nav" style="">
          <li class="hove">
            <a href="./dashboard.jsp">
              <i class="nc-icon nc-bank"></i>
              <p>Dashboard</p>
            </a>
          </li>
          
          <li class="hove">
            <a href="./postfounditem.jsp">
              <i class="nc-icon nc-sound-wave"></i>
              <p>Post Found Item</p>
            </a>
          </li>
          <li class="hove">
            <a href="./postlostitem.jsp">
              <i class="nc-icon nc-sun-fog-29"></i>
              <p>Post Lost Item</p>
            </a>
          </li>
          <li class="hove">
            <a href="./tracklost.jsp">
              <i class="nc-icon nc-alert-circle-i"></i>
              <p>Track Lost Item</p>
            </a>
          </li>
          <li class="hove">
            <a href="./trackfound.jsp">
              <i class="nc-icon nc-briefcase-24"></i>
              <p>Track Found Item</p>
            </a>
          </li>
         
          <li class="hove">
            <a href="./user.jsp">
              <i class="nc-icon nc-single-02"></i>
              <p>User Profile</p>
            </a>
          </li>
          <li class="hove">
            <a href="./contactus.jsp">
              <i class="nc-icon nc-tap-01"></i>
              <p>Contact Us</p>
            </a>
          </li>
          
          <li class="hove">
            <a href="http://localhost:8080/egertonfinder/Help/index.html">
              <i class="nc-icon nc-spaceship"></i>
              <p>Gain Experience</p>
            </a>
          </li>
          
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-toggle">
              <button type="button" class="navbar-toggler">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </button>
            </div>
            <a class="navbar-brand" href="#pablo">Egerton Lost and Found Systems</a> 
            <p style="color: forestgreen;font-size: 15px; font-style:italic">
               
               Welcome: <%= currentClient.getFirstname() + " " + currentClient.getLastname() %> </p>

          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end" id="navigation">
              
            <form>
              <div class="input-group no-border">
                <input type="text" value="" class="form-control" placeholder="Search...">
                <div class="input-group-append">
                  <div class="input-group-text">
                    <i class="nc-icon nc-zoom-split"></i>
                  </div>
                </div>
              </div>
            </form>
            <ul class="navbar-nav">
<!--              <li class="nav-item">
                <a class="nav-link btn-magnify" href="#pablo">
                  <i class="nc-icon nc-layout-11"></i>
                  <p>
                    <span class="d-lg-none d-md-block">Stats</span>
                  </p>
                </a>
              </li>-->
              <li class="nav-item btn-rotate dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="nc-icon nc-bell-55"></i>
                  <p>
                    <span class="d-lg-none d-md-block">Some Actions</span>
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="./notifications.jsp">Notifications</a>
                  <a class="dropdown-item" href="./user.jsp">Your Profile</a>
                  <a class="dropdown-item" href="./changepassword.jsp">Change Password</a>
                </div>
              </li>
              <li class="nav-item">
                <a class="nav-link btn-rotate" href="../logout">
                    <div id="onlogoutIcon">
                  <i class="nc-icon nc-button-power"></i>
                    </div>
                  <div id="onhoverLogout">Logout</div>
                  
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <!-- <div class="panel-header panel-header-lg">
  
  <canvas id="bigDashboardChart"></canvas>
  
  
</div> -->
      <div class="content">
        <div class="row">
            
          <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
              <div class="card-body ">
                <div class="row">
                  <div class="col-5 col-md-4">
                    <div class="icon-big text-center icon-warning">
                      <i class="nc-icon nc-globe text-warning"></i>
                    </div>
                  </div>
                  <div class="col-7 col-md-8">
                    <div class="numbers">
                      <p class="card-category">Found Items</p>
                      <p class="card-title">+10
                        <p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer ">
                <hr>
                <div class="stats">
                  <i class="fa fa-refresh"></i> Update Now
                </div>
              </div>
            </div>
          </div>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
              <div class="card-body ">
                <div class="row">
                  <div class="col-5 col-md-4">
                    <div class="icon-big text-center icon-warning">
                      <i class="nc-icon nc-vector text-danger"></i>
                    </div>
                  </div>
                  <div class="col-7 col-md-8">
                    <div class="numbers">
                      <p class="card-category">Lost Items</p>
                      <p class="card-title">+15
                        <p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer ">
                <hr>
                <div class="stats">
                  <i class="fa fa-clock-o"></i> In the last hour
                </div>
              </div>
            </div>
          </div>
            
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
              <div class="card-body ">
                <div class="row">
                  <div class="col-5 col-md-4">
                    <div class="icon-big text-center icon-warning">
                      <i class="nc-icon nc-favourite-28 text-primary"></i>
                    </div>
                  </div>
                  <div class="col-7 col-md-8">
                    <div class="numbers">
                      <p class="card-category">Tracked Items</p>
                      <p class="card-title">+3
                        <p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer ">
                <hr>
                <div class="stats">
                  <i class="fa fa-refresh"></i> Update now
                </div>
              </div>
            </div>
          </div>
        </div>
          
          
          <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Posted Lost Items</h4>
                <p class="card-category">Items that owners have lost and posted</p>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                    <%
                        
                        Connection conn=null;
                        Statement statement = null;
                        ResultSet resultSet = null;

                        %>
                    
                  <table class="table">
                    <thead class=" text-primary">
                    <th>
                        Property Id
                    </th>
                      <th>
                        Item Name
                      </th>
                      <th>
                        Place Lost
                      </th>
                      <th>
                        Date Lost
                      </th>                    
                      <!--<th class="text-right">-->
                      <th>
                        Status
                      </th>
                    </thead>
                                   <%
                                       
                                try{
                                     ConnectionManager manager= new ConnectionManager();  
                                     conn = manager.getConnect();
                                statement=conn.createStatement();
                                String sql ="select * from lostitems";
                                       // + " WHERE status='Pending'";
                                resultSet = statement.executeQuery(sql);
                                while(resultSet.next()){
                                %>
                                
                                
                    <tbody>
                      <tr>
                          <td>
                          <%=resultSet.getString("itemid") %>
                        </td>
                        <td>
                          <%=resultSet.getString("itemname") %>
                        </td>
                        <td>
                         <%=resultSet.getString("placelost") %>
                        </td>
                        <td>
                         <%=resultSet.getString("datelost") %>
                        </td>
                         <td>
                         <%=resultSet.getString("status") %>
                        </td>
                      </tr>
                      <%
                                }
                                conn.close();
                                } catch (Exception e) {
                                e.printStackTrace();
                                }
                                %>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="card card-plain">
              <div class="card-header">
                <h4 class="card-title"> Posted Found Items</h4>
                <p class="card-category">Items that have been found lost and posted</p>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                        <th>
                        Property Id
                      </th>
                     <th>
                        Item Name
                      </th>
                      <th>
                        Place Found
                      </th>
                      <th>
                        Date Found
                      </th>
                     <th>
                      Status
                      </th>
                    </thead>
                            
                    <%
                                try{
                                     ConnectionManager manager= new ConnectionManager();  
                                     conn = manager.getConnect();
                                statement=conn.createStatement();
                                String sql1 ="select * from founditems";
                                        //+ " WHERE status='Pending'";
                                resultSet = statement.executeQuery(sql1);
                                while(resultSet.next()){
                                    
                                %>
                    <tbody>
                      <tr>
                          <td>
                          <%=resultSet.getString("itemid") %>
                        </td>
                        <td>
                          <%=resultSet.getString("itemname") %>
                        </td>
                        <td>
                         <%=resultSet.getString("placefound") %>
                        </td>
                        <td>
                         <%=resultSet.getString("datefound") %>
                        </td>
                       
                         <td>
                            <%=resultSet.getString("status") %>
                        </td>
                      </tr>
                       <%
                                }
                                conn.close();
                                } catch (Exception e) {
                                e.printStackTrace();
                                }
                                %>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
              <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Tracked Items</h4>
                <p class="card-category">Items that have been traced and taken by owners</p>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                        
                      <th>
                        ID
                      </th>
                      <th>
                        Name
                      </th>
                      <th>
                        Description
                      </th>
                      <th>
                        Date Taken
                      </th>
                      <th>
                        Time Taken
                      </th>
                      <th>
                        More
                      </th>
                    </thead>
                    <%
                                try{
                                    
                                     ConnectionManager manager= new ConnectionManager();  
                                     conn = manager.getConnect();
                                statement=conn.createStatement();
                                String sql6 ="select * from tracked";
                                resultSet = statement.executeQuery(sql6);
                                    while(resultSet.next()){ 
                    %>
                    <tbody>
                      <tr>
                        <td>
                          <%=resultSet.getString("itemid") %>
                        </td>
                        <td>
                          <%=resultSet.getString("itemname") %>
                        </td>
                        <td>
                          <%=resultSet.getString("description") %>
                        </td>
                        
                        <td>
                          <%=resultSet.getString("datetaken") %>
                        </td>
                        <td>
                          <%=resultSet.getString("timetaken") %>
                        </td>
                        <td>
                            <a href="">Details</a>
                        </td>
                      </tr>
                      <%
                                }
                                conn.close();
                                } catch (Exception e) {
                                e.printStackTrace();
                                }
                                %>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
          
          
          
      </div>
      <footer class="footer footer-black  footer-white ">
        <div class="container-fluid">
          <div class="row">
            <nav class="footer-nav">
              <ul>
                <li>
                  <a href="#" target="_blank">Efinder Systems</a>
                </li>
                <li>
                  <a href="#" target="_blank">Blog</a>
                </li>
                <li>
                  <a href="#" target="_blank">Licenses</a>
                </li>
              </ul>
            </nav>
            <div class="credits ml-auto">
              <span class="copyright">
                ©
                <script>
                  document.write(new Date().getFullYear())
                </script>, made with <i class="fa fa-heart heart"></i> by Efinder Developers
              </span>
            </div>
          </div>
        </div>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <script src="../assets/js/core/jquery.min.js"></script>
  <script src="../assets/js/core/popper.min.js"></script>
  <script src="../assets/js/core/bootstrap.min.js"></script>
  <script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD-EqDa-aTpuUS0d9A6VCx8pH-kM6C3Q2k"></script>
  <!-- Chart JS -->
  <script src="../assets/js/plugins/chartjs.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="../assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="../assets/js/paper-dashboard.min.js?v=2.0.0" type="text/javascript"></script>
  <!-- Paper Dashboard DEMO methods, don't include it in your project! -->
  <script src="../assets/demo/demo.js"></script>
  <script>
    $(document).ready(function() {
      // Javascript method's body can be found in assets/assets-for-demo/js/demo.js
      demo.initChartsPages();
    });
  </script>
</body>

</html>
<% 
    }
    else{
         response.sendRedirect("http://localhost:8080/egertonfinder/errorpage.jsp?404 error! The page you are looking for could not be found");//404 error page will come here
     }
    %>