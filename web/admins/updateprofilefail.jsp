<%-- 
    Document   : user
    Created on : May 28, 2019, 12:40:25 PM
    Author     : Alex
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="admins.ConnectionManager"%>
<%
    String sids = (String)session.getAttribute("sid");
    String sfnames = (String)session.getAttribute("sfname");
    String slnames  = (String)session.getAttribute("slname");
    String smails = (String)session.getAttribute("smail");
    String sphones = (String)session.getAttribute("sphone");
    String snationals  = (String)session.getAttribute("snational");
    String sroles = (String)session.getAttribute("srole");
    %>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../content/images/img_467397.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../content/images/img_467397.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Egerton Lost and Lost Found Systems
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  <!-- CSS Files -->
  <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="../assets/css/paper-dashboard.css?v=2.0.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="../assets/demo/demo.css" rel="stylesheet" />
  <style>
     #onhoverLogout{
                visibility: hidden;
            }
            #onlogoutIcon:hover ~ #onhoverLogout{
                visibility: visible;
            }
    </style>
</head>

<body class="" onload="demo.showNotification27('top','center')">
  <div class="wrapper ">
    <div class="sidebar" data-color="green" data-active-color="danger">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
    -->
      <div class="logo">
        <a href="http://www.creative-tim.com" class="simple-text logo-mini">
          <div class="logo-image-small">
            <img src="../content/images/img_467397.png">
          </div>
        </a>
        <a href="http://www.creative-tim.com" class="simple-text logo-normal">
          Efinder
          <!-- <div class="logo-image-big">
            <img src="../assets/img/logo-big.png">
          </div> -->
        </a>
      </div>
      <div class="sidebar-wrapper">
       <ul class="nav" style="">
          <li class="hove">
            <a href="./dashboard.jsp">
              <i class="nc-icon nc-bank"></i>
              <p>Dashboard</p>
            </a>
          </li>
          
          <li class="hove">
            <a href="./postfounditem.jsp">
              <i class="nc-icon nc-sound-wave"></i>
              <p>Post Found Item</p>
            </a>
          </li>
          <li class="hove">
            <a href="./postlostitem.jsp">
              <i class="nc-icon nc-sun-fog-29"></i>
              <p>Post Lost Item</p>
            </a>
          </li>
          <li class="hove">
            <a href="./assignitems.jsp">
              <i class="nc-icon nc-bulb-63"></i>
              <p>Assign Items to Owners</p>
            </a>
          </li>
          <li class="hove">
            <a href="./entries.jsp">
              <i class="nc-icon nc-tile-56"></i>
              <p>System Entries</p>
            </a>
          </li>
  
          <li class="hove">
            <a href="./registerclients&pcs.jsp">
              <i class="nc-icon nc-single-copy-04"></i>
              <p>Register Clients & Pc's</p>
            </a>
          </li>
          <li class="hove">
            <a href="./user.jsp">
              <i class="nc-icon nc-single-02"></i>
              <p>User Profile</p>
            </a>
          </li>
          
          <li class="hove">
            <a href="http://localhost:8080/egertonfinder/Help/index.html">
              <i class="nc-icon nc-spaceship"></i>
              <p>Gain Experience</p>
            </a>
          </li>
        </ul>
      </div>
      </div>
    </div>
    <div class="main-panel" style="margin-top: -52%">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-toggle">
              <button type="button" class="navbar-toggler">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </button>
            </div>
            <a class="navbar-brand" href="#">Egerton Lost and Found Systems</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end" id="navigation">
            <form>
              <div class="input-group no-border">
                <input type="text" value="" class="form-control" placeholder="Search...">
                <div class="input-group-append">
                  <div class="input-group-text">
                    <i class="nc-icon nc-zoom-split"></i>
                  </div>
                </div>
              </div>
            </form>
           <ul class="navbar-nav">
<!--              <li class="nav-item">
                <a class="nav-link btn-magnify" href="#pablo">
                  <i class="nc-icon nc-layout-11"></i>
                  <p>
                    <span class="d-lg-none d-md-block">Stats</span>
                  </p>
                </a>
              </li>-->
              <li class="nav-item btn-rotate dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="nc-icon nc-bell-55"></i>
                  <p>
                    <span class="d-lg-none d-md-block">Some Actions</span>
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                 <a class="dropdown-item" href="./notifications.jsp">Notifications</a>
                  <a class="dropdown-item" href="./user.jsp">Your Profile</a>
                  <a class="dropdown-item" href="./changepassword.jsp">Change Password</a>
                </div>
              </li>
              <li class="nav-item">
                <a class="nav-link btn-rotate" href="../logout">
                    <div id="onlogoutIcon">
                  <i class="nc-icon nc-button-power"></i>
                    </div>
                  <div id="onhoverLogout">Logout</div>
                  
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <!-- <div class="panel-header panel-header-sm">
  
  
</div> -->
      <%
                        
                        Connection conn=null;
                        Statement statement = null;
                        ResultSet resultSet = null;
                           int newclientid = Integer.parseInt(sids); 
                        %>
                        
                        <%
                                       
                                try{
                                     ConnectionManager manager= new ConnectionManager();  
                                     conn = manager.getConnect();
                                statement=conn.createStatement();
                                String sql ="select * from users WHERE userId='"+newclientid+"'";
                                resultSet = statement.executeQuery(sql);
                                while(resultSet.next()){
                                    String F1= resultSet.getString("firstname");
                                    String F2= resultSet.getString("lastname");
                                    String Fmail= resultSet.getString("email");
                                    String Fphone= resultSet.getString("phone");
                                    String Fid= resultSet.getString("idNo");
                                    String Frole= resultSet.getString("role");
                                    String Fdepart= resultSet.getString("department");
                                
                         %>       
      <div class="content">
        <div class="row">
          <div class="col-md-4">
            <div class="card card-user">
              <div class="image">
                <img src="" alt="">
              </div>
              <div class="card-body">
                <div class="author">
                  <a href="#">
                    <img class="avatar border-gray" src="../assets/img/icons8-user-80.png" alt="...">
                     <h5 class="title"><%out.println(" "+F1 +" "+F2);%></h5>
                  </a>
                  <p class="descripti" style="text-align: left; font-size: 15px"> 
                      <strong>User ID:</strong> <%out.println(" "+sids);%> <br>
                      <strong>Role:</strong> <%out.println(" "+Frole);%> <br>
                      <strong>Department:</strong> <%out.println(" "+Fdepart);%><br>
                      <strong>Email:</strong> <%out.println(" "+Fmail);%> <br>
                      <strong>Phone:</strong> <%out.println(" "+Fphone);%> <br>
                      <strong>National ID:</strong> <%out.println(" "+Fid);%><br>
                      
                  </p>
                </div>
                <p class="description text-center">
                  
                  
                </p>
              </div>
              <div class="card-footer">
                <hr>
                <div class="button-container">
                  <div class="row">
                    <div class="col-lg-3 col-md-6 col-6 ml-auto">
                      <h5>
                        <br>
                        <small></small>
                      </h5>
                    </div>
                    <div class="col-lg-4 col-md-6 col-6 ml-auto mr-auto">
                      <h5>
                        <br>
                        <small></small>
                      </h5>
                    </div>
                    <div class="col-lg-3 mr-auto">
                      <h5>
                        <br>
                        <small></small>
                      </h5>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
          <div class="col-md-8">
           <div class="card card-user">
              <div class="card-header">
               <h5 class="card-title">Update Profile</h5>
                <p><i>Dear <%out.println(F1);%>,</i> note that you are only allowed to edit phone and email fields <strong style="color: red">ONLY.</strong></p>
              </div>
              <div class="card-body">
                  <form method="post" action="updateprofile.jsp">
                  <div class="row">
                    <div class="col-md-5 pr-1">
                      <div class="form-group">
                        <label>Account Name (disabled)</label>
                        <input type="text" class="form-control" disabled="" placeholder="Company" value="Efinder Systems">
                      </div>
                    </div>
                    <div class="col-md-3 px-1">
                      <div class="form-group">
                        <label>Role</label>
                        <input type="text" class="form-control" value="<%out.println(Frole);%>" readonly>
                      </div>
                    </div>

                  </div>
                  <div class="row">
                    <div class="col-md-6 pr-1">
                      <div class="form-group">
                        <label>First Name</label>
                        <input type="text" class="form-control" value="<%out.println(F1);%>" readonly>
                      </div>
                    </div>
                    <div class="col-md-6 pl-1">
                      <div class="form-group">
                        <label>Last Name</label>
                        <input type="text" class="form-control" value="<%out.println(F2);%>" minlength="4" maxlenth="30" readonly>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6 pr-1">
                      <div class="form-group">
                        <label>Phone Number</label>
                        <input type="text" class="form-control" name="sphone" value="<%out.println(Fphone);%>" minlength="10" maxlength="10" autocomplete="off" required="required">
                      </div>
                    </div>
                      <div class="col-md-6 pl-1">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" class="form-control" name="smail" value="<%out.println(Fmail);%>" autocomplete="off" minlength="11" maxlength="100" required="required">
                      </div>
                    </div>
                      
                  </div>
                    <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>National ID</label>
                        <input type="text" class="form-control" value="<%out.println(Fid);%>" minlength="12" maxlength="12" readonly>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="update ml-auto mr-auto">
                      <button type="submit" class="btn btn-primary btn-round">Update Profile</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
                      <%
                   
                   session.setAttribute("Fmail", Fmail);
                   session.setAttribute("Fphone", Fphone);
                   
                                }
                                conn.close();
                                } catch (Exception e) {
                                e.printStackTrace();
                                }
%>
      <footer class="footer footer-black  footer-white ">
        <div class="container-fluid">
          <div class="row">
            <nav class="footer-nav">
              <ul>
                <li>
                  <a href="#" target="_blank">Efinder</a>
                </li>
                <li>
                  <a href="#" target="_blank">Blog</a>
                </li>
                <li>
                  <a href="#" target="_blank">Licenses</a>
                </li>
              </ul>
            </nav>
            <div class="credits ml-auto">
              <span class="copyright">
                ©
                <script>
                  document.write(new Date().getFullYear())
                </script>, made with <i class="fa fa-heart heart"></i> by Efinder Developers
              </span>
            </div>
          </div>
        </div>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <script src="../assets/js/core/jquery.min.js"></script>
  <script src="../assets/js/core/popper.min.js"></script>
  <script src="../assets/js/core/bootstrap.min.js"></script>
  <script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD-EqDa-aTpuUS0d9A6VCx8pH-kM6C3Q2k"></script>
  <!-- Chart JS -->
  <script src="../assets/js/plugins/chartjs.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="../assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="../assets/js/paper-dashboard.min.js?v=2.0.0" type="text/javascript"></script>
  <!-- Paper Dashboard DEMO methods, don't include it in your project! -->
  <script src="../assets/demo/demo.js"></script>
</body>

</html>