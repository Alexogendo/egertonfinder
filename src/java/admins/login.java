/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admins;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Alex
 */
@WebServlet(name = "login", urlPatterns = {"/login"})
public class login extends HttpServlet {

    /**
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        try { 
        AdminBean admin = new AdminBean(); 
        admin.setEmail(request.getParameter("Email"));
        admin.setPassword(request.getParameter("Password")); 
        
        admin = AdminDao.login(admin);

     if (admin.isValid()&& (admin.getRole().equals("Manager"))){ 
         HttpSession session = request.getSession(true);
         //session.setMaxInactiveInterval(1800);  // 30 minute time out
         session.setAttribute("currentSessionAdmin", admin); 
         response.sendRedirect("manager/dashboard.jsp?Logged in successfully!"); //logged-in page 
     }
     else if(admin.isValid()&& (admin.getRole().equals("Securityadmin"))){
         HttpSession session1 = request.getSession(true);
        // session1.setMaxInactiveInterval(1800);  // 30 minute time out 
         session1.setAttribute("currentSessionSec", admin); 
         response.sendRedirect("admins/dashboard.jsp?Logged in successfully!"); //logged-in page 
     }
     else if(admin.isValid()&& ((admin.getRole().equals("Student")) || (admin.getRole().equals("Staff")) || (admin.getRole().equals("Subordinate")) || (admin.getRole().equals("Security")))){
         HttpSession session2 = request.getSession(true);
         //session2.setMaxInactiveInterval(1800);  // 30 minute time out 
         session2.setAttribute("currentSessionClient", admin); 
         response.sendRedirect("clients/dashboard.jsp?Logged in successfully!"); //logged-in page
     }
     else if(!admin.isValid()) {
        response.sendRedirect("loginfailed?Unable to login"); //error page will be changed 
        }
     else{
         response.sendRedirect("error?404 error! The page you are looking for could not be found");//404 error page will come here
     }
        } 
        catch (IOException | ClassNotFoundException theException) { 
            System.out.println(theException);
        } 
    }
}
