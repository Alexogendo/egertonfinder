/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admins;
import java.text.*;
import java.util.*;
import java.sql.*;
//import passwordhashanddecrytp.EncryptAndDecrypt;
/**
 *
 * @author Alex
 */
public class AdminDao {
  
    static Connection currentCon = null;
    static ResultSet rs = null;
    
    public static AdminBean login(AdminBean bean) throws ClassNotFoundException{
        
       
        //prepare some objects for connection
        Statement stmt = null;
        
        String username = bean.getEmail();
        String pass = bean.getPassword();
     
        
       String searchQuery = "select * from users where email='" + username + "' AND password='" + pass + "'";
        
             // "System.out.println" prints in the console; Normally used to trace the process 
             System.out.println("Your user name is " + username); 
             System.out.println("Your password is " + pass);
             System.out.println("Query: "+searchQuery);
             
        try{
            //connect to database
            
            currentCon = ConnectionManager.getConnect();
            stmt = currentCon.createStatement();
            rs =stmt.executeQuery(searchQuery);      
            boolean more = rs.next();
            
            // if user does not exist set the isValid variable to false 
            if (!more) { 
                System.out.println("Sorry, you are not a registered user! Please sign up first");
                bean.setValid(false); 
            } 
            //if user exists set the isValid variable to true 
            else if (more) { 
                String UserId = rs.getString("userId");
                String FirstName = rs.getString("firstname"); 
                String LastName = rs.getString("lastname"); 
                String emai = rs.getString("email");
                String pnum = rs.getString("phone"); 
                String naid = rs.getString("idNo"); 
                String rol= rs.getString("role");
                System.out.println("Welcome " + FirstName); 
                bean.setFirstname(FirstName); 
                bean.setLastname(LastName);
                bean.setRole(rol);
                bean.setId(UserId);
                bean.setEmail(emai);
                bean.setPhone(pnum);
                bean.setNationalId(naid);
                bean.setValid(true); 
            } else{
              bean.setValid(false);
               currentCon.close();
            }
        } 
               catch(SQLException ex){
            System.out.println("Login failed there are some errors: "+ ex);
             bean.setValid(false);
            
        }
         //close exception
        finally{
         if(rs!=null){
             try{
                 rs.close();
             }catch(SQLException ex2){}
                 rs = null;
                        
        }
        if(stmt != null ){
            try{
                stmt.close();
            }catch(SQLException ex2){}
            stmt = null;
        }
        
        if (currentCon != null ){
              try{
                currentCon.close();
            }catch(SQLException ex3){}
            currentCon = null;
        }
        }
         
        return bean;
    }
    public void logOut()
   {
     
   }
     
}
